package com.sub.sploosh.tests;


/**
 * @Author Liam Mullen
 *
 */

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.sub.sploosh.engine.Accelerometer;

import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class AccelerometerTest extends Activity implements SensorEventListener {

    SensorManager sm;
    Sensor accelerometer;
    TextView acceleration;
    SensorEvent event;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    // Create a MotionEvent representing a touch at touchPosition


    public static SensorEvent CreateSensorEvent(float value) {
        SensorEvent sensorEvent = Mockito.mock(SensorEvent.class);

        try {
            Field valuesField = SensorEvent.class.getField("values");
            valuesField.setAccessible(true);
            float[] sensorValue = {value};
            try {
                valuesField.set(sensorEvent, sensorValue);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return sensorEvent;
    }

    @Test
    public void TestMoveLeft() {
        SensorEvent event = CreateSensorEvent(10);
        double movement = Accelerometer.calculateMovement(event);
        assertTrue(movement < 0);
    }

    @Test
    public void TestMoveRight() {
        SensorEvent event = CreateSensorEvent(-10);
        double movement = Accelerometer.calculateMovement(event);
        assertTrue(movement > 0);
    }


    @Test
    public void TestNoMovement() {
        SensorEvent event = CreateSensorEvent(0);
        double movement = Accelerometer.calculateMovement(event);
        assertTrue(movement == 0);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
