package com.sub.sploosh.tests;

/**
 * Created by Fiona Mallett on 15/03/2016.
 */

import android.view.MotionEvent;

import com.sub.sploosh.engine.TouchInput;
import com.sub.sploosh.engine.math.Vector2;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertTrue;

public class TouchInputTest {


    private static MotionEvent createMotionEvent(Vector2 touchPosition) {
        MotionEvent motionEvent = Mockito.mock(MotionEvent.class);
        Mockito.when(motionEvent.getX()).thenReturn((float) touchPosition.getX());
        Mockito.when(motionEvent.getY()).thenReturn((float) touchPosition.getY());

        return motionEvent;
    }


    @Test
    public void testLeft() {

        //If touch input X value is less than half of the screen width, the player should move left
        assertTrue(TouchInput.getSpeed(createMotionEvent(new Vector2(0, 300)), new Vector2(400, 600)) == -1);
        assertTrue(TouchInput.getSpeed(createMotionEvent(new Vector2(100, 300)), new Vector2(400, 600)) == -1);
        assertTrue(TouchInput.getSpeed(createMotionEvent(new Vector2(200, 400)), new Vector2(400, 600)) == -1);

    }


    @Test
    public void testRight() {

        //If touch input X value is greater than half of the screen width, the player should move right
        assertTrue(TouchInput.getSpeed(createMotionEvent(new Vector2(201, 400)), new Vector2(400, 600)) == 1);
        assertTrue(TouchInput.getSpeed(createMotionEvent(new Vector2(300, 400)), new Vector2(400, 600)) == 1);
        assertTrue(TouchInput.getSpeed(createMotionEvent(new Vector2(400, 400)), new Vector2(400, 600)) == 1);

    }


}
