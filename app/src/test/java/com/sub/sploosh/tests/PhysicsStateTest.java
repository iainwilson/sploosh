package com.sub.sploosh.tests;

import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsState;
import com.sub.sploosh.engine.physics.PhysicsWorld;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by iain on 15/02/2016.
 */
public class PhysicsStateTest {

    private final PhysicsWorld physicsWorld = new PhysicsWorld();

    @Test
    public void testVelocity() {
        PhysicsState state = new PhysicsState(physicsWorld);
        state.setPosition(Vector2.ZERO);
        state.setVelocity(new Vector2(10, 10));

        // Simulate 1/2 a second passing
        state.update(500);

        assertEquals(state.position, new Vector2(5, 5));
    }

    @Test
    public void testAcceleration() {
        PhysicsState state = new PhysicsState(physicsWorld);
        state.setPosition(Vector2.ZERO);
        state.setVelocity(Vector2.ONE);
        state.setAcceleration(Vector2.ONE);

        state.update(500);

        assertEquals(new Vector2(1.5, 1.5), state.velocity);
        assertEquals(new Vector2(.75, .75), state.position);
    }

    @Test
    public void testWorldReference() {
        PhysicsState object1 = new PhysicsState(physicsWorld);
        PhysicsState object2 = new PhysicsState(physicsWorld);

        object1.setVelocity(new Vector2(0, +10)); // Moving downwards, speed 10
        object2.setVelocity(new Vector2(0, -10)); // Moving upwards, speed 10

        // Everything moves relative to object1
        physicsWorld.setReference(object1);

        // Simulate a second
        object1.update(1000);
        object2.update(1000);

        // Object one stays in the same position, despite having a non-zero velocity
        assertEquals(Vector2.ZERO, object1.position);
    }


}
