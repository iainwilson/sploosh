package com.sub.sploosh.tests;

import com.sub.sploosh.engine.math.MathUtilities;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by iain on 15/02/2016.
 */
public class MathUtilitiesTest {

    @Test
    public void testNormalizeAngle() {
        assertEquals(- 180, MathUtilities.normalizeAngle(-540), 0);
        assertEquals(-  90, MathUtilities.normalizeAngle(+270), 0);
        assertEquals(    0, MathUtilities.normalizeAngle(0), 0);
        assertEquals(    0, MathUtilities.normalizeAngle(+360), 0);
        assertEquals(    0, MathUtilities.normalizeAngle(-360), 0);
        assertEquals(+  90, MathUtilities.normalizeAngle(-270), 0);
        assertEquals(+ 180, MathUtilities.normalizeAngle(+540), 0);
    }

}
