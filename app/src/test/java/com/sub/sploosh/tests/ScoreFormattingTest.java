package com.sub.sploosh.tests;

import com.sub.sploosh.ScoresActivity;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by James Anderson on 16/03/16.
 */
public class ScoreFormattingTest
    {
    ScoresActivity scoresActivity = new ScoresActivity();

    @Test
    public void testOrdinals()
        {
            assertEquals("First Place = 1st!",     "1st!",  scoresActivity.getPlaceNumber(1));
            assertEquals("First Place = 10th!",    "10th", scoresActivity.getPlaceNumber(10));
            assertEquals("Eleventh Place = 11th",  "11th", scoresActivity.getPlaceNumber(11));
            assertEquals("Twelfth Place = 12th",   "12th", scoresActivity.getPlaceNumber(12));
            assertEquals("Thirteenth Place = 13th","13th", scoresActivity.getPlaceNumber(13));
        }

    @Test
    public void testScoreFormatting()
        {
            assertEquals(          "1",           "1",         scoresActivity.formatScore(1));
            assertEquals(         "10",          "10",        scoresActivity.formatScore(10));
            assertEquals(        "100",         "100",       scoresActivity.formatScore(100));
            assertEquals(      "1,000",       "1,000",      scoresActivity.formatScore(1000));
            assertEquals(     "10,000",      "10,000",     scoresActivity.formatScore(10000));
            assertEquals(    "100,000",     "100,000",    scoresActivity.formatScore(100000));
            assertEquals(  "1,000,000",   "1,000,000",   scoresActivity.formatScore(1000000));
            assertEquals( "10,000,000",  "10,000,000",  scoresActivity.formatScore(10000000));
            assertEquals("100,000,000", "100,000,000", scoresActivity.formatScore(100000000));

        }
    }
