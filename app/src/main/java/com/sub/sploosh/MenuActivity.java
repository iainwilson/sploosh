package com.sub.sploosh;

import android.app.Activity;
import android.os.Bundle;

public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Add the demo details fragment, listing all available demos
        setContentView(R.layout.activity_menu);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new MenuFragment()).commit();
        }
    }
}
