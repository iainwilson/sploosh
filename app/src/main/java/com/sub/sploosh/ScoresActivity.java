package com.sub.sploosh;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sub.sploosh.engine.ScoreManager;

import java.text.NumberFormat;
import java.util.List;

/**
 * Activity that displays the top 10 high scores
 *
 * @author James Anderson
 */
public class ScoresActivity extends AppCompatActivity
    {
    ScoreManager scoreManager;
    List<ScoreManager.Score> scores;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            //Use a list view to fill the screen
            listView = new ListView(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            this.addContentView(listView, layoutParams);
            setContentView(listView);
            setTitle("High Scores!");

            //Access the score database
            scoreManager = new ScoreManager(this);
            scores = scoreManager.getScores();

            //use an adapter to put the score data into a list view
            listView.setAdapter(new BaseAdapter()
            {
            @Override
            public int getCount()
                {
                    return scores.size();
                }

            @Override
            public ScoreManager.Score getItem(int position)
                {
                    return scores.get(position);
                }

            @Override
            public long getItemId(int position)
                {
                    return 0;
                }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent)
                {
                    final ScoreManager.Score score = getItem(position);

                    TextView view = new TextView(ScoresActivity.this);
                    view.setPadding(50, 25, 50, 25);

                /*
                 * This formats the output string for the displayed score on a line
                 * formatScore() adds commas to the integer to make it look correct based on the users locale (location)
                 * then the user's name is printed
                 * 'position' comes from the getView parameter, you need to add one as the first position is zero
                 */
                    view.setText(String.format("%-10s %s%n%17s", getPlaceNumber(position + 1), score.name.trim(), formatScore(score.score).trim()));
                    view.setTextSize(4 * getResources().getDisplayMetrics().density);

                    Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "font/code-new-roman-b.otf");
                    view.setTypeface(tf);

                    //Show a toast of the position that the score is in
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                        {
                            Context context = getApplicationContext();
                            CharSequence text = getPlaceNumber(position + 1);
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                        }
                    });
                    return view;
                }
            });
        }

    /**
     * @param i the number that you want to get the place number of
     * @return a String of the formatted number
     * @see <a href="http://stackoverflow.com/questions/6810336/is-there-a-library-or-utility-in-java-to-convert-an-integer-to-its-ordinal">http://stackoverflow.com/</a>
     */
    public static String getPlaceNumber(int i)
        {
            //List of all the potential suffixed (letters on the end of the number)
            String[] sufixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
            //This case statement allows for the fact that 11th, 12th and 13th don't follow the normal rule
            //I have left this in to make the code reusable
            switch (i % 100)
                {
                    case 11:
                    case 12:
                    case 13:
                        return i + "th";
                    default:
                        //The rest of the integers follow the rule in the suffixes String[] above
                        //Exclamation mark is printed on first place
                        if (i == 1)
                            {
                                //two spaces before start
                                return i + sufixes[i % 10] + "!";
                            }

                        //This helps with the formatting of the first 9 numbers
                        if (i < 10)
                            {
                                //two spaces before start
                                return i + sufixes[i % 10];
                            }
                        //This case is really for 10th
                        //one space before start
                        return i + sufixes[i % 10];
                }
        }

    /**
     * @param i the number you wish to format with ',' or '.' etc depending on locale
     * @return the formatted number based on the users <i>Locale</i>
     * (<i>Locale</i> - some countries format their numbers differently, getInstance()
     * checks the default locale and looks after this for us)
     * @see NumberFormat
     */
    public String formatScore(int i)
        {
            String formattedNumber = NumberFormat.getInstance().format(i);
            return formattedNumber;
        }

    }