package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

public class PlatformSpawner extends ObstacleSpawner<Platform> {

    public PlatformSpawner(Game game) {
        super(game);
    }

    @Override
    protected int getMaximumEntities() {
        return 1;
    }

    @Override
    protected boolean shouldRemove(Platform entity) {
        if (!game.getMonkeyEntity().isAlive()) {
            return true;
        }
        return entity.getPosition().getY() < 0;
    }

    @Override
    protected Platform spawn() {
        return new Platform(game);
    }

}
