package com.sub.sploosh.game;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.sub.sploosh.R;
import com.sub.sploosh.engine.Game;

/**
 * Created by iain on 13/03/16.
 */
public class GameOverMenuFragment extends Fragment {

    private ViewGroup fragmentLayout;
    private LinearLayout controlsLayout;
    private EditText nameTextField;

    // Actions triggered by this fragment
    public interface GameOverMenuActionListener {
        void onSaveScore(String name);
        void onPlayAgainClicked();
        void onBackClicked();
    }

    private GameOverMenuActionListener listener;

    public GameOverMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_game_over_menu, container, false);

        this.fragmentLayout = (ViewGroup) view.findViewById(R.id.frameLayout);
        this.controlsLayout = (LinearLayout) fragmentLayout.findViewById(R.id.linearLayout);

        //fragment_game_over_menu.xml ensures that the max length is 20 characters
        this.nameTextField = (EditText) controlsLayout.findViewById(R.id.nameTextField);

        Button playAgainButton = (Button) controlsLayout.findViewById(R.id.playAgainButton);
        Button backButton = (Button) controlsLayout.findViewById(R.id.backButton);

        playAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionallySaveScore();
                nameTextField.clearFocus();
                listener.onPlayAgainClicked();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionallySaveScore();
                nameTextField.clearFocus();
                listener.onBackClicked();
            }
        });

        return view;
    }

    private void optionallySaveScore() {
        String name = nameTextField.getText().toString();


        if (Game.godMode)
            {
                return;
            }
        if (TextUtils.isEmpty(name)) {
            return;
        }

//        nameTextField.setInputType(InputType.TYPE_NULL);
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(nameTextField.getWindowToken(), 0);

        listener.onSaveScore(name);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onAttach((Context) activity);
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof GameOverMenuActionListener) {
            listener = (GameOverMenuActionListener) context;
        }
        else {
            throw new RuntimeException(context.toString()
                    + " must implement GameOverMenuActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

}
