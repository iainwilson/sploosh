package com.sub.sploosh.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.sub.sploosh.engine.Animation;
import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.audio.Sound;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;
import com.sub.sploosh.engine.physics.PhysicsWorld;

public class Monkey extends PhysicsEntity {

    private final Game game;
    private float rotation = 0;

    private final Vector2 maxVelocity, minVelocity;
    private final Vector2 balloonSize;

    private static Bitmap fallingSprite;
    private static Bitmap dyingSprite;

    private static Animation fallingAnimation;
    private static Animation dyingAnimation;
    private static Sound splooshSound;
    private static Sound munch;

    //* The monkey will have a few states: falling, dying, shield and balloon.

    protected MonkeyState state;

    public abstract class MonkeyState {
        public abstract void update(Game game, long elapsed);
        public abstract void render(Game game, Renderer renderer);
    }

    /**
     * Pre loads assets into game
     * @param game
     *
     * @author James
     */
    public static void preLoad(Game game) {
        fallingSprite = game.getAssetManager().loadAndAddAndGetBitmap("movingMonkey", "img/crazymonkey.png");
        fallingAnimation = new Animation(fallingSprite, new Vector2(242, 245), 100);

        dyingSprite = game.getAssetManager().loadAndAddAndGetBitmap("explodeMonkey", "img/explodeMonkey.png");
        dyingAnimation = new Animation(dyingSprite, new Vector2(176, 145), 250);

        splooshSound = game.getAssetManager().loadAndAddAndGetSound("sploosh", "audio/sploosh.mp3");
        munch = game.getAssetManager().loadAndAddAndGetSound("bananaSound", "audio/munch.mp3");
    }

    private static final int[] instanceOffsets = new int[] { -1, 0, +1 };
    private final Vector2.Mutable instancePosition = new Vector2.Mutable();
    private final Rect instanceRectangle = new Rect();

    /*
     * The basic playing state, the monkey will fall, if it collides with
     * an obstacle it will enter the dying state (die). If it collides with a shield or balloon,
     * it will enter these states.
     *
     * @author Iain
     */
    private class MonkeyFallingState extends MonkeyState {

        private Animation.Tracker fallingAnimationTracker = fallingAnimation.createTracker();

        @Override
        public void update(Game game, long elapsed) {
            fallingAnimationTracker.advance(elapsed);

            if (game.hasBegun()) {
                double factor = (maxVelocity.getY() - physicsState.velocity.getY()) / maxVelocity.getY();

                // Don't allow the speed the monkey falls at to exceed the maximum
                // Acceleration decreases when approaching the maximum velocity
                physicsState.setAcceleration(
                        factor * PhysicsWorld.GRAVITY.getX(),
                        factor * PhysicsWorld.GRAVITY.getY()
                );
            }
        }

        @Override
        public void render(Game game, Renderer renderer) {
            double gameWidth = renderer.getBounds().width();
            double halfMonkeyWidth = getSize().getX() / 2;

            Vector2 position = getPosition();

            /*
             * Other instances of the monkey might need to be rendered:
             * One the width of the game to the left, and one the width of the game to the right.
             *
             * A maximum of two of these can be visible at any time, when the monkey is half off
             * one edge and half on the other
             */
            for (double xOffset : instanceOffsets) {
                xOffset *= gameWidth;

                instancePosition.set(position.getX() + xOffset, position.getY());

                // Be efficient and don't bother to render if this instance of the monkey isn't visible
                boolean visible = (instancePosition.getX() > -halfMonkeyWidth) && (instancePosition.getX() < (gameWidth + halfMonkeyWidth));

                if (visible) {
                    renderInstance(renderer, instancePosition);
                }
            }
        }

        // Renders the monkey bitmap at the given position
        protected void renderInstance(Renderer renderer, Vector2 position) {
            Canvas canvas = renderer.getCanvas();

            // Push graphics context
            canvas.save();
            canvas.rotate(rotation, (float) position.getX(), (float) position.getY());

            instanceRectangle.set(getRectangle());
            instanceRectangle.offsetTo((int) position.getX(), (int) position.getY());
            instanceRectangle.offset(-(int) (getSize().getX() / 2), -(int) (getSize().getY() / 2));

            fallingAnimationTracker.draw(renderer.getCanvas(), instanceRectangle);

            // Pop graphics context
            canvas.restore();
        }
    }

    /**
     * @author Iain
     */
    public Monkey(Game game) {
        super(game.getPhysicsWorld());
        this.game = game;

        Renderer renderer = game.getRenderer();

        // Make the monkey 1/5th the width of the screen with height scaled accordingly
        setSize(renderer.getScaledSize(fallingAnimation.getFrameSize(), 1. / 4.5, null));

        this.balloonSize = renderer.getScaledSize(Renderer.getBitmapSize(Balloon.balloon), 1. / 5., null);

        // Move right to test wrapping from one side of the screen to the other
        physicsState.setVelocity(new Vector2(0, game.getRenderer().getSize().getY() / 2));

        // Place the monkey halfway across and 15% of the way down the screen
        physicsState.setPosition(renderer.getScaledPosition(new Vector2(0.5, 0.15)));

        Vector2 gameSize = renderer.getSize();

        // Maximum speed of falling the height of the screen in one second,
        // and moving across the entire screen in 1.5 seconds
        this.maxVelocity = new Vector2(gameSize.getX() * 1.5, gameSize.getY());

        this.minVelocity = new Vector2(0, gameSize.getY() / 5);

        // Monkey starts off falling by default
        this.state = new MonkeyFallingState();
    }

    private long age = 0;

    @Override
    /**
     * @author Iain
     */
    public void update(Game game, long elapsed) {
        Renderer renderer = game.getRenderer();

        final Vector2 gameSize = renderer.getSize();

        // Update velocity X component
        physicsState.setVelocity(maxVelocity.getX() * game.getInput().getMovement(), physicsState
                .velocity.getY());

        // Do update specific to whatever state the monkey is in
        state.update(game, elapsed);

        // Do physics update
        super.update(game, elapsed);

        Rect bounds = renderer.getBounds();

        // Detect and adjust the the monkey's position when moving off the edge of the screen
        {
            double width = bounds.width();
            Vector2 position = getPosition();

            // Check for monkey's position being off left or right side
            if (position.getX() >= bounds.right || position.getX() <= bounds.left) {
                double x = position.getX();

                // Correct position to opposite side of the screen
                x = (x < bounds.left) ? (width + x) : (x - width);

                // Update with corrected position
                this.physicsState.setPosition(x, position.getY());
            }
        }

        // Some rotation to make the monkey appear to be floating about a bit
        {
            age += elapsed;

            // Monkey rocks from side to side every 3 seconds
            double period = 3000;

            rotation = (float) (15.f * Math.sin((age % period) / period * 2 * Math.PI));
        }
    }

    /**
     * @author Fiona and Iain
     */
    private class MonkeyShieldState extends MonkeyFallingState {


        //If the monkey picks up a shield, it will not be able to die but if it collides with an
        // obstacle, they will lose the power up.
        @Override
        public void update(Game game, long elapsed) {
            timeElapsed += elapsed;

            //creating a shield which will pulse around the monkey
            double phase = (Math.sin(timeElapsed / 1000.0 * Math.PI) + 1) / 2.0;
            radiusMultiplier = 0.5 + phase * 0.25;

            super.update(game, elapsed);
        }

        private Paint shieldPaint = new Paint(0);

        //drawing the shield
        {
            shieldPaint.setColor(0x8066ff5a);
            shieldPaint.setStrokeWidth(5);
        }

        private long timeElapsed = 0;
        private double radiusMultiplier = 1;

        @Override
        protected void renderInstance(Renderer renderer, Vector2 position) {
            //creating the pulse illusion using radiusMultiplier
            renderer.getCanvas().drawCircle((float)position.getX(), (float)position.getY(), (float)(getSize().getY() * radiusMultiplier), shieldPaint);
            super.renderInstance(renderer, position);
        }
    }

    /*
     * @author Fiona and Iain
     *
     * If the monkey catches the hot air balloon, the falling speed will slow down, making it
     * easier to avoid obstacles. This state will last for some time unless the player
     * dies or collides with a shield.
     */
    private class MonkeyBalloonState extends MonkeyFallingState {

        private long totalTime = 0, timeLimit = 5000, transitionTime = 500;
        private double scale = 0;
        private double offsetY = 0;
        private Sound.Playback deflatePlayback;
        private final Vector2 acceleration = PhysicsWorld.GRAVITY.times(-5);

        MonkeyBalloonState() {
            super();

            Balloon.balloonInflate.play();
        }

        @Override
        public void update(Game game, long elapsed) {

            super.update(game, elapsed);
            physicsState.setAcceleration(acceleration);

            totalTime += elapsed;

            if (physicsState.velocity.getY() <= minVelocity.getY()) {
                //the velocity can't be less than zero or the monkey will start going backwards
                physicsState.setVelocity(physicsState.velocity.getX(), minVelocity.getY());
                physicsState.setAcceleration(PhysicsWorld.GRAVITY);
            }

            // Transition in
            if (totalTime <= transitionTime) {
                scale = (double) totalTime / transitionTime;
            }

            // Transition out
            if (totalTime >= timeLimit - transitionTime) {
                if (deflatePlayback == null) {
                    deflatePlayback = Balloon.balloonDeflate.play();
                }

                // Control the scale and positioning during the transition out animation to make it
                // appear that the balloon flies off into the background
                scale = 1. - (double) (totalTime - (timeLimit - transitionTime)) / transitionTime;
                offsetY = -4 * (1. - scale);
            }

            // Once the time is up the monkey will lose the hot air balloon
            if (totalTime >= timeLimit) {
                state = new MonkeyFallingState();
            }

        }

        private RectF rect = new RectF();

        @Override
        protected void renderInstance(Renderer renderer, Vector2 position) {
            Vector2.calculateSizeCenteredAt(balloonSize, position, rect);

            // Offset the balloon position so the monkey appears to hang from it
            rect.offset(0, (float) ((-0.5 + offsetY) * balloonSize.getY()));

            // Render the balloon
            Canvas canvas = renderer.getCanvas();
            canvas.save();
            canvas.rotate(rotation, (float) position.getX(), (float) position.getY());

            if (scale < 1) {
                canvas.scale((float) scale, (float) scale, (float) position.getX(), (float) position.getY());
            }

            canvas.drawBitmap(Balloon.balloon, null, rect, null);
            canvas.restore();

            // Render the monkey
            super.renderInstance(renderer, position);
        }
    }

    /**
     * @author Iain
     */
    private class MonkeyDyingState extends MonkeyState {

        private Animation.Tracker dyingAnimationTracker = dyingAnimation.createTracker();
        private final Vector2.Mutable acceleration = new Vector2.Mutable(PhysicsWorld.GRAVITY.negated().times(10));
        private final Vector2.Mutable explosionSize = new Vector2.Mutable(getSize());

        public MonkeyDyingState()
        {
            splooshSound.play();
        }

        @Override
        public void update(Game game, long elapsed) {
            dyingAnimationTracker.advance(elapsed);

            // Cause the movement to decelerate to a stop in both x and y
            double accelerationX, accelerationY;
            accelerationY = -10 * PhysicsWorld.GRAVITY.getY();

            if (physicsState.velocity.getX() <= 0) {
                accelerationX = 0;
            }
            else {
                accelerationX = Math.signum(physicsState.velocity.getX()) * maxVelocity.getX();
            }

            acceleration.set(accelerationX, accelerationY);
            physicsState.setAcceleration(acceleration);

            // Make the explosion grow
            explosionSize.multiplyBy(1.2);
            setSize(explosionSize);

            if (physicsState.velocity.getY() <= 0) {
                // Finished dying
                physicsState.setAcceleration(Vector2.ZERO);
                physicsState.setVelocity(Vector2.ZERO);
                state = new MonkeyDeadState();

                // Remove the monkey and display game over
                game.getLoop().deferredDetach(Monkey.this);
                game.getLoop().deferredAttach(new GameOver(game));
            }
        }

        @Override
        public void render(Game game, Renderer renderer) {
            dyingAnimationTracker.draw(renderer.getCanvas(), getRectangle());
        }
    }

    /**
     * Nothing to actually do in this state, just indicates the game is over
     */
    private class MonkeyDeadState extends MonkeyState {

        @Override
        public void update(Game game, long elapsed) {}

        @Override
        public void render(Game game, Renderer renderer) {}

    }

    public void render(Game game, Renderer renderer) {
        // Run rendering code for whatever state the monkey's in
        state.render(game, renderer);

        super.render(game, renderer);
    }

    /**
     * @author Fiona
     */
    @Override
    public void collidedWith(PhysicsEntity e)
    {
        // Disable collision detection while dying or dead
        if (!isAlive()) {
            return;
        }

        //When the monkey collides with a shield, they will be protected to hit one more obstacle,
        //then will return to the normal state (when they hit another obstacle, they die)
        if (e instanceof Shield) {
            this.state = new MonkeyShieldState();
            game.getLoop().detach(e); //removing the shield
            return;
        }

        if (e instanceof Rocket || e instanceof Platform || e instanceof ThunderCloud || e instanceof Bird) {
            if (this.state instanceof MonkeyShieldState) {
                game.getLoop().detach(e); //removing the platform or rocket when the monkey hits it
                state = new MonkeyFallingState(); //back to original state
            } else {
                kill();
            }
            return;
        }

        if (e instanceof Balloon) {
            this.state = new MonkeyBalloonState();
            game.getLoop().detach(e);
            return;
        }
        if (e instanceof Banana) {
            game.getLoop().detach(e);
            munch.play();
            //update score 500 points
            game.getScoreKeeper().setScore();
            return;
        }
    }

    @Override
    public Rect getCollisionRectangle() {
        if (isAlive()) {
            return super.getCollisionRectangle();
        }

        return null;
    }

    public boolean isAlive() {
        return !(state instanceof MonkeyDyingState || state instanceof MonkeyDeadState);
    }

    private void kill() {
        if (Game.godMode) {
            return;
        }

        // If monkey collides with a collision, only update the state once (can only die once)
        if (isAlive()) {
            this.state = new MonkeyDyingState();
        }
    }

}