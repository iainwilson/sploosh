package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.MathUtilities;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

/**
 * Created by Fiona Mallett on 07/03/2016.
 */
public class Banana extends PhysicsEntity{


    public Banana(Game game) {
        super(game.getPhysicsWorld());
        Vector2 gameSize = game.getRenderer().getSize();
        this.bitmap = game.getAssetManager().loadAndAddAndGetBitmap("banana", "banana.png");
        setSize(game.getRenderer().getScaledSize(Renderer.getBitmapSize(bitmap), 1.0 / 10.0, null));
        this.physicsState.setPosition(MathUtilities.randomInRange(1, 19) / 20.0 * gameSize.getX(), gameSize.getY());
    }

    @Override
    public void render(Game game, Renderer renderer) {
        super.render(game, renderer);
    }

    @Override
    public void update(Game game, long elapsed) {
        super.update(game, elapsed);
    }

}
