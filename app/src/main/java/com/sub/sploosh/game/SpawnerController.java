package com.sub.sploosh.game;

import com.sub.sploosh.engine.Entity;
import com.sub.sploosh.engine.Game;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Jason on 23/02/16.
 */

/**
 * The Spawner Controller is responsible for controlling the sequence that spawners activate in,
 * and which spawenrs are allowed to be concurrently active
 */
public class SpawnerController extends Entity {

    Random randomInt;

    final Spawner[] obstacleSpawners;
    final Spawner[] powerUpSpawners;
    final Set<Spawner> activeSpawners = new HashSet<>();

    private final Spawner platformSpawner;
    private final Spawner rocketSpawner;
    private final Spawner birdSpawner;
    private final Spawner shieldSpawner;
    private final Spawner balloonSpawner;
    private final Spawner bananaSpawner;
    private final Spawner thunderCloudSpawner;

    public SpawnerController(Game game) {
        randomInt = new Random();

        // Overrides shouldSpawn() methods in all the spawners to additionally check that they are active spawners
        platformSpawner = new PlatformSpawner(game) {
            @Override
            protected boolean shouldSpawn() {
                return isActiveSpawner(this) && super.shouldSpawn();
            }
        };

        birdSpawner = new BirdSpawner(game) {
            @Override
            protected boolean shouldSpawn() {
                return isActiveSpawner(this) && super.shouldSpawn();
            }
        };

        rocketSpawner = new RocketSpawner(game) {
            @Override
            protected boolean shouldSpawn() {
                return isActiveSpawner(this) && super.shouldSpawn();
            }
        };

        thunderCloudSpawner = new ThunderCloudSpawner(game) {
            @Override
            protected boolean shouldSpawn() {
                return isActiveSpawner(this) && super.shouldSpawn();
            }
        };

        shieldSpawner = new ShieldSpawner(game) {
            @Override
            protected boolean shouldSpawn() {
                return isActiveSpawner(this) && super.shouldSpawn();
            }
        };

        balloonSpawner = new BalloonSpawner(game) {
            @Override
            protected boolean shouldSpawn() {
                return isActiveSpawner(this) && super.shouldSpawn();
            }
        };

        bananaSpawner = new BananaSpawner(game) {
            @Override
            protected boolean shouldSpawn() {
                return isActiveSpawner(this) && super.shouldSpawn();
            }
        };

        // An array of obstacle spawners
        obstacleSpawners = new Spawner[]{
                rocketSpawner,
                birdSpawner,
                thunderCloudSpawner
        };

        // An array of power ups
        powerUpSpawners = new Spawner[]{
                shieldSpawner,
                balloonSpawner
        };

        // Obstacles and power ups are added to the array
        for (Spawner spawner: obstacleSpawners)
        {
            game.getLoop().attach(spawner);
        }

        for (Spawner spawner: powerUpSpawners)
        {
            game.getLoop().attach(spawner);
        }

        game.getLoop().attach(platformSpawner);
        game.getLoop().attach(thunderCloudSpawner);

        game.getLoop().attach(bananaSpawner);

        // Bananas are alawys active at the beginning of every game
        activate(bananaSpawner);
    }

    public void activate(Spawner spawner)
    {
        activeSpawners.add(spawner);
    }

    public void deactivate(Spawner spawner)
    {
        activeSpawners.remove(spawner);
    }

    public boolean isActiveSpawner(Spawner spawner)
    {
        return activeSpawners.contains(spawner);
    }

    long timePassed = 0;
    long currentRandomPhaseRemaining = 0;

    @Override
    public void update(Game game, long elapsed)
    {
        long timeAfterUpdate = timePassed + elapsed;

        // Beginning of linear spawn sequence
        // At 2 seconds activate platforms
        if (timePassed < 2000 && timeAfterUpdate >= 2000)
        {
            activate(platformSpawner);
        }
        // At 10 seconds deactive platforms, and activate birds and balloons
        else if (timePassed < 10000 && timeAfterUpdate >= 10000)
        {
            deactivate(platformSpawner);

            activate(birdSpawner);
            activate(balloonSpawner);

        }
        // At 20 seconds deactivate birds and balloons, and activate rockets and shields
        else if (timePassed < 20000 && timeAfterUpdate >= 20000)
        {
            deactivate(birdSpawner);
            deactivate(balloonSpawner);

            activate(rocketSpawner);
            activate(shieldSpawner);
        }
        // At 30 seconds deactivate rockets and shields, and activate thunder clouds
        else if (timePassed < 30000 && timeAfterUpdate >= 30000)
        {
            deactivate(rocketSpawner);
            deactivate(shieldSpawner);

            activate(thunderCloudSpawner);
        }
        // Beginning of random spawn sequence
        else if (timePassed >= 40000)
        {
            /*
             a new random phase will begin if it is the first random phase,
             or when the time of the current random phase has elapsed
              */
            if (currentRandomPhaseRemaining <= 0)
            {
                // deactivate all spawners at beginning of current random phase
                activeSpawners.clear();

                // Constant activation of platform obstacle and banana power up
                activate(platformSpawner);
                activate(bananaSpawner);

                // Activate a random second obstacle and power up
                activeSpawners.add(obstacleSpawners[randomInt.nextInt(obstacleSpawners.length)]);
                activeSpawners.add(powerUpSpawners[randomInt.nextInt(powerUpSpawners.length)]);

                // If the thundercloud is active, deactivate the platforms
                if (isActiveSpawner(thunderCloudSpawner))
                {
                    deactivate(platformSpawner);
                }

                /* Removed as potentially made the game too difficult
                if (timePassed >= 50000)
                {
                    //Chance of activating a random third obstacle
                    activeSpawners.add(obstacleSpawners[randomInt.nextInt(obstacleSpawners.length)]);
                }
                */

                // Each phase of the random spawn sequence is 5 seconds long
                currentRandomPhaseRemaining = 5000;
            }

            currentRandomPhaseRemaining -= elapsed;
        }

        timePassed = timeAfterUpdate;
    }

}