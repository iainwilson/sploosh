package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

/**
 * Created by Jason on 24/02/16.
 */
public class BirdSpawner extends ObstacleSpawner<Bird> {

    private long timePassed;

    public BirdSpawner(Game game)
    {
        super(game);
    }

    @Override
    protected Bird spawn() {
        return new Bird(game);
    }

    @Override
    protected boolean shouldSpawn() {
        if (timePassed < 2000) {
            return false;
        }

        timePassed = 0;
        return super.shouldSpawn();
    }

    @Override
    protected boolean shouldRemove(Bird entity) {
        return entity.getRectangle().bottom < 0;
    }

    @Override
    protected int getMaximumEntities() {
        return 1;
    }

    @Override
    public void update(Game game, long elapsed) {
        timePassed += elapsed;

        super.update(game, elapsed);
    }
}
