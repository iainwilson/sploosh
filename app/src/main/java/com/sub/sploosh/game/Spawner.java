package com.sub.sploosh.game;

import com.sub.sploosh.engine.Entity;
import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.GameLoop;
import com.sub.sploosh.engine.RenderableEntity;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.util.Utilities;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Spawns and controls the lifespan of a number of entities
 * @param <T> Type of entity that this spawner spawns. Must extend RenderableEntity
 */
public abstract class Spawner<T extends RenderableEntity> extends RenderableEntity {

    protected final Game game;

    public Spawner(Game game) {
        this.game = game;
    }

    // Currently active entities
    protected final LinkedList<T> entities = new LinkedList<>();

    /**
     * Iterator over all the entities managed by the spawner
     */
    @Override
    public Iterator<Entity> iterator() {
        List<? extends Entity> entityList = entities;
        return (Iterator<Entity>) entityList.iterator();
    }

    @Override
    public void update(Game game, long elapsed) {
        final GameLoop loop = game.getLoop();

        // Remove any expired entities
        Iterator<T> iterator = entities.iterator();

        while (iterator.hasNext()) {
            T entity = iterator.next();

            if (shouldRemove(entity)) {
                iterator.remove();

                // Track removal
                // System.out.printf("Spawner: %s removing entity %s%n", Utilities.getClassName(this), entity);
            }
        }

        // Generate more entities as needed
        while (!hasMaximumEntities() && shouldSpawn()) {
            T entity = spawn();
            entities.add(entity);

            // Track spawns
            // System.out.printf("Spawner: %s spawning entity %s%n", Utilities.getClassName(this), entity);
        }

        // Update all active entities
        for (T entity : entities) {
            entity.update(game, elapsed);
        }
    }

    @Override
    public void render(Game game, Renderer renderer) {
        // Render all active entities
        for (T entity : entities) {
            entity.render(game, renderer);
        }
    }

    /**
     * Override to create and return a new entity
     */
    protected abstract T spawn();

    /**
     * Override with some logic to determine when a new entity needs to be spawned
     * Default is to allow spawning while the player is still alive
     */
    protected boolean shouldSpawn() {
        return game.getMonkeyEntity().isAlive();
    }

    /**
     * Override with some logic to determine when an entity's lifetime has ended
     */
    protected abstract boolean shouldRemove(T entity);

    /**
     * Override to return the maximum number of entities that can exist at once
     */

    protected abstract int getMaximumEntities();

    private boolean hasMaximumEntities() {
        return entities.size() >= getMaximumEntities();
    }

    /**
     * Entity that has lived the longest
     */
    protected T getOldestEntity() {
        if (entities.isEmpty()) {
            return null;
        }

        return entities.get(0);
    }

    /**
     * Entity that has lived the shortest
     */
    protected T getNewestEntity() {
        if (entities.isEmpty()) {
            return null;
        }

        return entities.get(entities.size() - 1);
    }

}
