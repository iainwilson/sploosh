package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

/**
 * Created by Liam Mullen on 25/02/2016.
 */
public class BalloonSpawner extends ObstacleSpawner<Balloon> {

    private long timePassed;

    public BalloonSpawner(Game game) {
        super(game);
    }
    @Override
    protected Balloon spawn() {
        return new Balloon(game);
    }

    @Override
    protected boolean shouldSpawn() {
        if (timePassed > 4000) {
            timePassed = 0;
            return super.shouldSpawn();
        }

        return false;
    }

    @Override
    protected boolean shouldRemove(Balloon entity) {
        return entity.getPosition().getY() < 0;
    }

    @Override
    protected int getMaximumEntities() {
        return 1;
    }

    @Override
    public void update(Game game, long elapsed) {
        timePassed += elapsed;

        super.update(game, elapsed);
    }
}
