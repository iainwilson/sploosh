package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

public class CloudSpawner extends Spawner<Cloud> {

    private static final int MAXIMUM = 3;

    // How far up the screen the previous cloud must be before spawning another
    private static final double yThreshold = ((MAXIMUM - 1.f) / MAXIMUM);

    public CloudSpawner(Game game) {
        super(game);
    }

    @Override
    protected boolean shouldSpawn() {
        Cloud newest = getNewestEntity();

        // Spawn more clouds when the most recently spawned has moved far enough up the screen
        return (newest == null) || (newest.getRectangle().bottom < (game.getRenderer().getBounds().bottom * yThreshold));
    }

    @Override
    protected boolean shouldRemove(Cloud cloud) {
        // Remove a cloud once offscreen
        return cloud.getRectangle().bottom < 0;
    }

    @Override
    protected Cloud spawn() {
        return new Cloud(game);
    }

    @Override
    protected int getMaximumEntities() {
        return MAXIMUM;
    }

}
