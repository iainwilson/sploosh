package com.sub.sploosh.game;

import android.graphics.Color;
import android.graphics.Paint;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.MathUtilities;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

public class Platform extends PhysicsEntity {

    private static final Paint paint = new Paint();

    {
        paint.setColor(Color.RED);
    }

    public Platform(Game game) {
        super(game.getPhysicsWorld());

        Vector2 gameSize = game.getRenderer().getSize();
        Vector2 size = new Vector2(gameSize.getX() / 5.0, gameSize.getY() / 50.0);
        setSize(size);

        double halfWidth = getSize().getX() / 2;

        // Randomise spawn position
        this.physicsState.setPosition(
            MathUtilities.randomInRange((int) halfWidth, (int) (Renderer.WIDTH - halfWidth)),
            Renderer.HEIGHT
        );
    }


    @Override
    public void render(Game game, Renderer renderer) {
        renderer.getCanvas().drawRect(this.getRectangle(), paint);

        super.render(game, renderer);
    }

}
