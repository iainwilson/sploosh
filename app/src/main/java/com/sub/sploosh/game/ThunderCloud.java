package com.sub.sploosh.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.audio.Sound;
import com.sub.sploosh.engine.math.MathUtilities;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

/**
 * Joint effort between Liam and Fiona on 02/03/2016.
 */
public class ThunderCloud extends PhysicsEntity {

    private static final Vector2 scale = new Vector2(3.6, 2.6);
    private final int radius = MathUtilities.randomInRange((int) (Renderer.WIDTH / 15), (int)
            (Renderer.WIDTH / 10));

    private final Paint paintSky = new Paint();
    private long timePassed = 3500;
    private double maximumSpeed;
    private Vector2 ep = new Vector2(0, 0);
    private static Sound spark;
    private Rect darkScreen = new Rect();

    /**
     * @author Liam
     */
//    //Previously drew our own thundercloud
//    {
//        int gray = MathUtilities.randomInRange(220, 255);
//        int alpha = MathUtilities.randomInRange(192, 255);
//        paint.setARGB(alpha, gray, gray, gray);
//        paint.setAntiAlias(true);
//    }
//

    /**
     * @author Fiona
     */

//    //Previously provided glow around the cloud before we changed it to an image
//    private final Paint paintBlur = new Paint();
//
//    {
//        int gray = MathUtilities.randomInRange(220, 255);
//        int alpha = MathUtilities.randomInRange(192, 255);
//        paint.setARGB(alpha, gray, gray, gray);
//        paint.setAntiAlias(true);
//        paintBlur.setColor(Color.BLUE);
//        paintBlur.setMaskFilter(new BlurMaskFilter(50, BlurMaskFilter.Blur.NORMAL));
//    }
    public ThunderCloud(Game game) {
        super(game.getPhysicsWorld());
        Vector2 gameSize = game.getRenderer().getSize();
        this.bitmap = game.getAssetManager().loadAndAddAndGetBitmap("thundercloud", "thuncloud.png");
        setSize(game.getRenderer().getScaledSize(Renderer.getBitmapSize(bitmap), 0.4, null));
        this.maximumSpeed = game.getRenderer().getSize().getY();
        this.physicsState.setPosition(gameSize.getX() / 2, gameSize.getY() / 1.1);
        this.physicsState.setVelocity(gameSize.getX() / 0.5f, game.getMonkeyEntity().getPhysicsState().velocity.getY());
        spark = game.getAssetManager().loadAndAddAndGetSound("sparkSound", "audio/spark.mp3");
    }


    /**
     * @author Fiona and Iain
     */
    @Override
    public void render(Game game, Renderer renderer) {

        Vector2 gameSize = game.getRenderer().getSize();
        //  this.physicsState.setPosition(gameSize.getX() / 2, gameSize.getY() + 10);

        //   Vector2 position = physicsState.position;
        // this.physicsState.setVelocity(gameSize.getX() / 2, gameSize.getY() + 10);
        Canvas canvas = renderer.getCanvas();

        Vector2 ep;

        //Only generate lightning bolts while the player is alive
        if (game.getMonkeyEntity().isAlive()) {


            if (timePassed > 2000 && timePassed <= 4000 && bolt != null) {

                bolt.render(game, renderer);


            }
        }

        this.physicsState.setPosition(physicsState.position.getX(), gameSize.getY() / 1.1);


        //Darken the whole screen when a thundercloud appears
        darkScreen = (game.getRenderer().getBounds());
        paintSky.setColor(Color.BLACK);
        paintSky.setAlpha(50);
        renderer.getCanvas().drawRect(game.getRenderer().getBounds(), paintSky);

        super.render(game, renderer);
    }

    /**
     * @author Liam
     */
    public Bolt shootBolt(Game game, Vector2 enp) {
        Vector2 gameSize = game.getRenderer().getSize();
//       Vector2 ep = new Vector2(MathUtilities.randomInRange(0, (int)gameSize.getX()),game.getMonkeyEntity().getPosition().getY() );
        Bolt bt = new Bolt(physicsState.position, enp);
        return bt;
    }

    private Bolt bolt;

    /**
     * @author Liam, Iain and Fiona
     */
    @Override
    public void update(Game game, long elapsed) {

        timePassed += elapsed;


        if (timePassed > 4550) {
            timePassed = 0;
            spark.play();
            this.physicsState.setVelocity(0, game.getMonkeyEntity().getPhysicsState().velocity.getY());
            ep = new Vector2(MathUtilities.randomInRange(0, (int) game.getRenderer().getSize()
                    .getX()), game.getMonkeyEntity().getPosition().getY());

            bolt = shootBolt(game, ep);
        }

        if (timePassed > 1800 && bolt != null) {
            bolt.update(game, elapsed);
            Vector2 killPoint = bolt.getKillPoint();
            //Shield doesn't protect monkey from dying with the bolt yet
            // game.getMonkeyEntity().state instanceof MonkeyShieldState
            if (game.getMonkeyEntity().getRectangle().contains((int) killPoint.getX(), (int) killPoint.getY())) {
                game.getMonkeyEntity().collidedWith(this);
            }


            super.update(game, elapsed);
        }


        //POSITIONS
        Rect gameBounds = game.getRenderer().getBounds();
        Vector2 position = getPosition();

        //Thunder cloud bounce off the screen but keeping its Y values
        if (position.getX() >= gameBounds.right || position.getX() <= gameBounds.left) {
            //negate the velocity
            physicsState.setVelocity(-physicsState.velocity.getX(), physicsState.velocity.getY());
        }

        super.update(game, elapsed);
    }
}
