package com.sub.sploosh.game;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.sub.sploosh.R;
import com.sub.sploosh.engine.Renderer;

public class GameMenuFragment extends Fragment {

    // Defines events that this fragment triggers
    public interface GameMenuActionListener {
        void onPlayClicked();
        void onHighScoresClicked();
        void onDemosClicked();
        void onToggleControls(boolean useAccelerometer);
        void onToggleFrameRate(boolean state);
        void onToggleDebugFlag(Renderer.DebugFlag flag, boolean state);
        void onToggleGodMode(boolean state);
    }

    protected ViewGroup fragmentLayout;
    protected LinearLayout controlsLayout;

    private GameMenuActionListener listener;

    public GameMenuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_game_menu, container, false);
        this.fragmentLayout = (ViewGroup) view.findViewById(R.id.frameLayout);
        this.controlsLayout = (LinearLayout) fragmentLayout.findViewById(R.id.linearLayout);

        // Allows us to reference XML Buttons
        Button playButton        = (Button) controlsLayout.findViewById(R.id.playButton);
        Button demosButton       = (Button) controlsLayout.findViewById(R.id.demosButton);
        Switch tiltControlSwitch = (Switch) controlsLayout.findViewById(R.id.tiltControlSwitch);
        Switch fpsCounterSwitch  = (Switch) controlsLayout.findViewById(R.id.fpsCounterSwitch);
        Button highScoresButton  = (Button) controlsLayout.findViewById(R.id.highScoresButton);
        Switch debugBoundsSwitch  = (Switch) controlsLayout.findViewById(R.id.debugBoundsSwitch);
        Switch debugHitboxesSwitch  = (Switch) controlsLayout.findViewById(R.id.debugHitboxesSwitch);
        Switch debugCollisionsSwitch  = (Switch) controlsLayout.findViewById(R.id.debugCollisionsSwitch);
        Switch godModeSwitch  = (Switch) controlsLayout.findViewById(R.id.godModeSwitch);

        // Set up event listeners and triggers
        demosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDemosClicked();
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPlayClicked();
            }
        });

        highScoresButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHighScoresClicked();
            }
        });

        attachPreferenceToSwitch(Preference.tiltControl, tiltControlSwitch, new Preference.ChangeListener() {
            @Override
            public void onChanged(boolean value) {
                listener.onToggleControls(value);
            }
        });

        attachPreferenceToSwitch(Preference.frameRate, fpsCounterSwitch, new Preference.ChangeListener() {
            @Override
            public void onChanged(boolean value) {
                listener.onToggleFrameRate(value);
            }
        });

        attachPreferenceToSwitch(Preference.debugRenderer, debugBoundsSwitch, new Preference.ChangeListener() {
            @Override
            public void onChanged(boolean value) {
                listener.onToggleDebugFlag(Renderer.DebugFlag.Bounds, value);
            }
        });

        attachPreferenceToSwitch(Preference.debugHitboxes, debugHitboxesSwitch, new Preference.ChangeListener() {
            @Override
            public void onChanged(boolean value) {
                listener.onToggleDebugFlag(Renderer.DebugFlag.Hitboxes, value);
            }
        });

        attachPreferenceToSwitch(Preference.debugCollisions, debugCollisionsSwitch, new Preference.ChangeListener() {
            @Override
            public void onChanged(boolean value) {
                listener.onToggleDebugFlag(Renderer.DebugFlag.Collisions, value);
            }
        });

        attachPreferenceToSwitch(Preference.godMode, godModeSwitch, new Preference.ChangeListener() {
            @Override
            public void onChanged(boolean value) {
                listener.onToggleGodMode(value);
            }
        });

        return view;
    }

    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            super.onAttach(context);
            onAttachToContext(context);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }

    /*
     * This method will be called from one of the two previous method
     */
    protected void onAttachToContext(Context context) {
        if (context instanceof GameMenuActionListener) {
            listener = (GameMenuActionListener) context;
        }
        else {
            throw new RuntimeException(context.toString()
                    + " must implement GameMenuActionListener");
        }
    }

    public static void attachPreferenceToSwitch(final Preference preference, final CompoundButton button, final Preference.ChangeListener changeHandler) {
        final CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preference.set(isChecked);
            }
        };

        preference.setListener(new Preference.ChangeListener() {
            @Override
            public void onChanged(boolean value) {
                button.setOnCheckedChangeListener(null);

                button.setChecked(value);
                changeHandler.onChanged(value);

                button.setOnCheckedChangeListener(listener);
            }
        });
    }

}
