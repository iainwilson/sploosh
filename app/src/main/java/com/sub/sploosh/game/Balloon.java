package com.sub.sploosh.game;

import android.graphics.Bitmap;
import android.graphics.Rect;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.audio.Sound;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;


/**
 * Joint effort by Fiona, Liam and Iain on 25/02/2016.
 */
public class Balloon extends PhysicsEntity {

    private double maximumSpeed;

    public static Bitmap balloon;
    public static Sound balloonInflate, balloonDeflate;

    /**
     * @author Iain
     */
    public static void preLoad(Game game) {
        balloon = game.getAssetManager().loadAndAddAndGetBitmap("balloon", "hotairballoon.png");
        balloonInflate = game.getAssetManager().loadAndAddAndGetSound("balloon-inflate", "audio/balloon-inflate.wav");
        balloonDeflate = game.getAssetManager().loadAndAddAndGetSound("balloon-deflate", "audio/balloon-deflate.wav");
    }

    /**
     * @author Liam and Fiona
     */
    public Balloon(Game game) {
        super(game.getPhysicsWorld());
        Vector2 gameSize = game.getRenderer().getSize();
        this.bitmap = balloon;

        setSize(game.getRenderer().getScaledSize(Renderer.getBitmapSize(bitmap), 0.1, null));

        //Maximum speed is moving the height of the game in one second
        this.maximumSpeed = game.getRenderer().getSize().getY();
        this.physicsState.setPosition(gameSize.getX() / 5, gameSize.getY());

        //The balloon will bounce off the screen at smaller angles so the player has a chance to collide with it
        this.physicsState.setVelocity(gameSize.getX() / 1.5f, 0.75 * game.getMonkeyEntity().getPhysicsState().velocity.getY());
    }

    @Override
    public void render(Game game, Renderer renderer) {
        super.render(game, renderer);
    }

    /**
     * @author Liam, Iain and Fiona
     */
    @Override
    public void update(Game game, long elapsed) {

        Rect gameBounds = game.getRenderer().getBounds();
        Vector2 position = getPosition();

        //When the balloon hits either side of the screen, it will change direction
        if (position.getX() >= gameBounds.right || position.getX() <= gameBounds.left) {
            //negate the velocity
            physicsState.setVelocity(-physicsState.velocity.getX(), physicsState.velocity.getY());
        }

        super.update(game, elapsed);
    }

}
