package com.sub.sploosh.game;

import android.graphics.Rect;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.physics.PhysicsEntity;

/**
 * Created by Liam Mullen on 18/02/2016.
 */
public abstract class ObstacleSpawner<T extends PhysicsEntity> extends Spawner<T> {
    public ObstacleSpawner(Game game) {
        super(game);
    }

    @Override
    public void update(Game game, long elapsed) {
        super.update(game, elapsed);

        Monkey monk = game.getMonkeyEntity();
        Rect monkRec =  monk.getCollisionRectangle();

        for (PhysicsEntity obstacle : entities) {
            Rect obstacleCollisionRectangle = obstacle.getCollisionRectangle();

            if (monkRec == null || obstacleCollisionRectangle == null) {
                // Collisions disabled for this obstacle
                continue;
            }

            // Use Rect.intersects here because Rect#intersect modifies the Rect
            // it is called on, which can lead to a subtle bug
            if (Rect.intersects(monkRec, obstacleCollisionRectangle)) {
                if (game.getRenderer().isDebugging(Renderer.DebugFlag.Collisions)) {
                    game.pause();
                }

                // Inform both entities involved to handle the collision
                monk.collidedWith(obstacle);
                obstacle.collidedWith(monk);

                break;
            }

        }
    }
}
