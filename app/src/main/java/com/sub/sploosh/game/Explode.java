package com.sub.sploosh.game;

import com.sub.sploosh.engine.Animation;
import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

/**
 * Created by Fiona Mallett on 17/02/2016.
 */
public class Explode extends PhysicsEntity {


    private Animation.Tracker animationTracker;

    public Explode(Game game) {
        super(game.getPhysicsWorld());

        //Load and cache explosion sprite sheet
        this.bitmap = game.getAssetManager().loadAndAddAndGetBitmap("explodeMonkey", "img/explodeMonkey.png");

        // Define an animation from the flyingSprite bitmap, giving the size of each frame and duration of
        // each frame in milliseconds
        Animation animation = new Animation(this.bitmap, new Vector2(176, 145), 50);
        animationTracker = animation.createTracker();

        setSize(game.getRenderer().getScaledSize(animation.getFrameSize(), 0.5, null));

        //spawn the animation over the position of the monkey
        this.physicsState.setPosition(game.getRenderer().getScaledPosition(new Vector2(0.5, 0.5)));
    }


    @Override
    public void update(Game game, long elapsed) {
        super.update(game, elapsed);
        animationTracker.advance(elapsed);

    }

    @Override
    public void render(Game game, Renderer renderer) {
        animationTracker.draw(renderer.getCanvas(), getRectangle());
        super.render(game, renderer);

    }
}
