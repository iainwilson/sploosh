package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

/**
 * Created by Fiona Mallett on 24/02/2016.
 */
public class ShieldSpawner extends ObstacleSpawner<Shield> {

    private long timePassed;


    public ShieldSpawner(Game game) {
        super(game);
    }

    @Override
    protected Shield spawn() {

        return new Shield(game);
    }

    @Override
    protected boolean shouldSpawn() {
        //Spawning every 5 seconds
        if (timePassed > 5000) {
            timePassed = 0;
            return super.shouldSpawn();
        }

        return false;
    }

    @Override
    protected boolean shouldRemove(Shield entity) {

        if (!game.getMonkeyEntity().isAlive()) //if dead, remove shield
        {
            return true;
        }
        return entity.getPosition().getY() < 0; //off the screen
    }

    @Override
    //Only allow one shield on the screen at any one time
    protected int getMaximumEntities() {
        return 1;
    }

    @Override
    public void update(Game game, long elapsed) {
        timePassed += elapsed;

        super.update(game, elapsed);
    }
}




