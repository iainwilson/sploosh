package com.sub.sploosh.game;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.RenderableEntity;
import com.sub.sploosh.engine.Renderer;

/**
 * Created by Jason on 18/02/16.
 */
public class ScoreKeeper extends RenderableEntity {

    private final Paint scorePainter = new Paint();
    private final double TIME_MODIFIER = 3000.0;

    private int currentScore = 0;

    public ScoreKeeper() {
        scorePainter.setColor(Color.WHITE);
        scorePainter.setTextSize(30);
    }

    @Override
    public void update(Game game, long elapsed) {
        Monkey monkey = game.getMonkeyEntity();

        if (monkey.isAlive()) {
            double monkeyVelocity = monkey.getPhysicsState().velocity.getY();

            // The elapsed time is divided by a modifier constant, and then multiplied by the monkey's current velocity
            currentScore += (elapsed / TIME_MODIFIER) * monkeyVelocity;
        }
    }

    @Override
    public void render(Game game, Renderer renderer) {
        Rect screen = renderer.getBounds();

        String message = String.format("Score: %d", currentScore);
        renderer.getCanvas().drawText(message, screen.left, screen.top + scorePainter.getTextSize(), scorePainter);
    }


    public int getScore() {
        return currentScore;
    }

    //Banana powerup
    //when the player collects a banana this method will be called to gain 500 points
    public int setScore() {
        return currentScore += 500;
    }
}
