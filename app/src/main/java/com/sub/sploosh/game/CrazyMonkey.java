package com.sub.sploosh.game;

import android.graphics.Canvas;

import com.sub.sploosh.engine.Animation;
import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.RenderableEntity;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.Vector2;

public class CrazyMonkey extends RenderableEntity {

    private Animation.Tracker animationTracker;
    private long age;
    private double rotation = 0;

    public CrazyMonkey(Game game) {
        this.bitmap = game.getAssetManager().loadAndAddAndGetBitmap("crazymonkey", "img/crazymonkey.png");

        Animation animation = new Animation(this.bitmap, new Vector2(280, 280), 100);
        animationTracker = animation.createTracker();

        setSize(game.getRenderer().getScaledSize(animation.getFrameSize(), 0.25, null));
        setPosition(game.getRenderer().getScaledPosition(new Vector2(0.5, 0.5)));
    }

    @Override
    public void update(Game game, long elapsed) {

        // Some rotation to make the monkey appear to be floating about a bit
        {
            age += elapsed;

            // Monkey rocks from side to side every 3 seconds
            double period = 3000;

            // Ranges from 0 to 1
            double howFarThroughRotation = (age % period) / period;

            // Ranges from -1 to +1
            double progress = Math.sin(Math.toRadians(360) * howFarThroughRotation);

            rotation = (float) (15.f * progress);
        }

        animationTracker.advance(elapsed);

        super.update(game, elapsed);
    }

    @Override
    public void render(Game game, Renderer renderer) {
        Canvas canvas = renderer.getCanvas();

        canvas.save();
        canvas.rotate((float) rotation, (int) getPosition().getX(), (int)getPosition().getY());
        animationTracker.draw(renderer.getCanvas(), getRectangle());
        canvas.restore();
    }
}