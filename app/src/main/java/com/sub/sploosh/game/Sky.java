package com.sub.sploosh.game;

import android.graphics.Paint;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.RenderableEntity;
import com.sub.sploosh.engine.Renderer;

public class Sky extends RenderableEntity {

    private static final Paint paint = new Paint();

    {
        // Sky blue
        paint.setColor(0xff81c2ea);
    }

    @Override
    public void render(Game game, Renderer renderer) {
        renderer.setBackgroundPaint(paint);
    }

}
