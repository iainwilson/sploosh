package com.sub.sploosh.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.MathUtilities;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

public class Cloud extends PhysicsEntity {

    private static final Paint paint = new Paint();

    private int gray = MathUtilities.randomInRange(220, 255);
    private int alpha = MathUtilities.randomInRange(192, 255);

    private static final int minRadius = (int) (Renderer.WIDTH / 15);
    private static final int maxRadius = (int) (Renderer.WIDTH / 10);

    // Individual clouds vary randomly between a minimum and maximum size
    private final int radius = MathUtilities.randomInRange(minRadius, maxRadius);
    private final double scale = (double) radius / minRadius;

    // A shape that all clouds use as a template, varying in scale
    private static final Path shape = new Path();
    private static final RectF bounds = new RectF();

    // Generate a single path for all clouds from 4 overlapping circles
    static {
        // Positions and scales of smaller circles that form the cloud
        float factors[][] = {
            {  0,  0,    1    },
            {  0, -0.6f, 1    },
            { -1,  0,    0.8f },
            {  1,  0,    0.8f }
        };

        for (float[] f : factors) {
            // left, top and radius
            float x = f[0], y = f[1], r = f[2];

            Path subpath = new Path();
            subpath.addCircle(x * minRadius, y * minRadius, r * minRadius, Path.Direction.CW);
            subpath.close();

            shape.op(subpath, Path.Op.UNION);
        }

        shape.computeBounds(bounds, true);
    }

    public Cloud(Game game) {
        super(game.getPhysicsWorld());

        Vector2 size = new Vector2(bounds.width(), bounds.height()).times(scale);
        setSize(size);

        Rect screen = game.getRenderer().getBounds();

        int x = MathUtilities.randomInRange(0, screen.width());
        int y = screen.bottom + (int) size.getY();

        Vector2 position = new Vector2(x, y);
        Vector2 velocity = new Vector2(MathUtilities.randomInRange(-100, 100), 0);

        physicsState.setPosition(position);
        physicsState.setVelocity(velocity);

        invalidateBounds();
    }

    @Override
    public void render(Game game, Renderer renderer) {
        Vector2 position = physicsState.position;

        Canvas canvas = renderer.getCanvas();

        canvas.save();
        canvas.translate((float) position.getX(), (float) position.getY());
        canvas.scale((float) scale, (float) scale);

        paint.setColor(Color.argb(alpha, gray, gray, gray));
        canvas.drawPath(shape, paint);

        canvas.restore();

        super.render(game, renderer);
    }

}
