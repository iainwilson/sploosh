package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

/**
 * Created by Fiona Mallett on 07/03/2016.
 */
public class BananaSpawner extends ObstacleSpawner<Banana> {

    private static final long interval = 1500;

    // Spawns immediately
    private long timePassed = interval;
    private boolean spawnedOnce;

    public BananaSpawner(Game game) {
        super(game);
    }

    @Override
    protected Banana spawn() {
        return new Banana(game);
    }

    @Override
    protected boolean shouldSpawn() {
        //using timePassed to prevent more than one banana on screen at one time
        if (timePassed >= interval) {
            timePassed = 0;
            return super.shouldSpawn();
        }

        return false;
    }

    @Override
    protected boolean shouldRemove(Banana entity) {

        if (!game.getMonkeyEntity().isAlive()) {
            return true;
        } else return entity.getPosition().getY() < 0;
    }


    @Override
    protected int getMaximumEntities() {
        return 1;
    }

    @Override
    public void update(Game game, long elapsed) {
        timePassed += elapsed;

        super.update(game, elapsed);
    }

}
