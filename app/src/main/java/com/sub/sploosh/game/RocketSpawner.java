package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

/**
 * Created by Fiona Mallett on 17/02/2016.
 */
public class RocketSpawner extends ObstacleSpawner<Rocket> {

    private long timePassed;

    public RocketSpawner(Game game) {
        super(game);
    }

    @Override
    protected Rocket spawn() {

        return new Rocket(game);
    }

    @Override
    protected boolean shouldSpawn() {
        if (timePassed <= 2000) {
            return false;
        }

        // Every 2 seconds a new rocket will spawn
        timePassed = 0;
        return super.shouldSpawn();
    }

    @Override
    protected boolean shouldRemove(Rocket entity) {
        return entity.isDead();
    }

    @Override
    //Only allow 1 rocket on the screen
    protected int getMaximumEntities() {
        return 1;
    }

    @Override
    public void update(Game game, long elapsed) {
        timePassed += elapsed;

        super.update(game, elapsed);
    }
}
