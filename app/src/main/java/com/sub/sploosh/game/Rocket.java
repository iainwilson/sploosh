package com.sub.sploosh.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

import com.sub.sploosh.engine.Animation;
import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.audio.Sound;
import com.sub.sploosh.engine.math.MathUtilities;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

public class Rocket extends PhysicsEntity {

    /**
     * Load the rocket flyingSprite sheet as a bitmap via the asset manager
     * Keep this static so it is only loaded once
     */
    private static Bitmap flyingSprite, dyingImage;

    /**
     * Define an animation from the flyingSprite bitmap, giving the size of each frame and duration
     * of each frame in milliseconds
     */
    private static Animation animation;

    /**
     * declare the rocket flying and dying sounds, these are pre loaded later on to
     * improve performance of the rocket
     */
    private static Sound flyingSound;
    private static Sound dyingSound;

    private Sound.Playback flyingSoundPlayback;

    /**
     * Loads Images and music before the game begins playing. Improves rocket performance
     * @param game
     */
    public static void preLoad(Game game) {
        flyingSprite = game.getAssetManager().loadAndAddAndGetBitmap("rocket", "rocket.png");
        animation = new Animation(flyingSprite, new Vector2(175, 370), 50);
        dyingImage = game.getAssetManager().loadAndAddAndGetBitmap("rocket-dying", "rocket-dying.png");
        dyingSound = game.getAssetManager().loadAndAddAndGetSound("rocketDrop", "audio/rocketDrop.mp3");
        flyingSound = game.getAssetManager().loadAndAddAndGetSound("rocketNoise", "audio/rocket.wav");
    }

    /**
     *For this instance of the rocket, create an animation tracker which will keep track of the
     * current frame of the flyingSprite sheet as it is updated
     */
    private Animation.Tracker animationTracker = animation.createTracker();

    private double minimumSpeed, maximumSpeed;

    // How quickly the rocket can change direction (turn)
    // in degrees per millisecond.
    private double maximumTurnRate = 90.0 / 1000.0; // 90 degrees in a second

    private final Vector2 collisionRectCenter = new Vector2(0, -0.25);
    private final double collisionRectSize;
    private final Rect collisionRectangle = new Rect();
    private final Matrix collisionMatrix = new Matrix();

    /**
     * Default constructor required with these parameters, for each class extending PhysicsEntity
     * @param game
     */
    public Rocket(Game game) {
        super(game);

        Vector2 gameSize = game.getRenderer().getSize();

        this.maximumSpeed = gameSize.getY();
        this.minimumSpeed = this.maximumSpeed / 10;

        // Make the rockets 1/6th the width of the game
        this.setSize(game.getRenderer().getScaledSize(animation.getFrameSize(), 1. / 6., null));
        this.collisionRectSize = getSize().getX() * 0.75;

        double minX = gameSize.getX() * 0.2;
        double maxX = gameSize.getX() * 0.8;
        double randomX = gameSize.getX() / MathUtilities.randomInRange(-5, 5);
        double positionX = game.getMonkeyEntity().getPosition().getX() + randomX;
        positionX = Math.max(Math.min(positionX, maxX), minX);

        // Spawn the rocket horizontally under the monkey and off the bottom of the screen
        this.physicsState.setPosition(
            positionX,
            gameSize.getY() * 1.2
        );

        // Spawn the rocket aiming at the target (monkey)
        PhysicsEntity target = game.getMonkeyEntity();

        Vector2 course = target.getPosition().minus(this.getPosition()).times(1. / MathUtilities.randomInRange(1, 10));
        this.physicsState.setVelocity(course);

        // Rocket is initially in the "flying state" of its life
        this.state = new RocketFlyingState();
    }

    private void die() {
        state = new RocketDyingState();

        if (flyingSoundPlayback != null) {
            flyingSoundPlayback.stop();
        }
    }

    public boolean isDead() {
        // If the rocket is dead and off the screen, return true for the rocket to be removed
        // using the shouldRemove() method of the rocket entity class
        return (state instanceof RocketDyingState && getPosition().getY() > Renderer.HEIGHT);
    }

    @Override
    // Called automatically each frame when this object is attached to the game loop
    public void update(Game game, long elapsed) {
        // Delegate updating to whatever state the rocket is currently in
        state.update(game, elapsed);

        //This must go at the end of this method to update parameters related to the superclass (Physics Entity)
        super.update(game, elapsed);
    }

    private float[] mappedCollisionPoints = new float[2];

    @Override
    protected void invalidateBounds() {
        super.invalidateBounds();

        // Need to update the collision rectangle after moving
        collisionMatrix.reset();

        collisionMatrix.setRotate(
                (float) physicsState.velocity.direction(),
                (float) getPosition().getX(),
                (float) getPosition().getY()
        );

        // Apply transform to collision hitbox
        mappedCollisionPoints[0] = (float) (getPosition().getX() + getSize().getX() * collisionRectCenter.getX());
        mappedCollisionPoints[1] = (float) (getPosition().getY() + getSize().getY() * collisionRectCenter.getY());
        collisionMatrix.mapPoints(mappedCollisionPoints);

        double collisionRectCenterX = mappedCollisionPoints[0];
        double collisionRectCenterY = mappedCollisionPoints[1];

        collisionRectangle.set(
                (int) (collisionRectCenterX - (collisionRectSize / 2)),
                (int) (collisionRectCenterY - (collisionRectSize / 2)),
                (int) (collisionRectCenterX + (collisionRectSize / 2)),
                (int) (collisionRectCenterY + (collisionRectSize / 2))
        );
    }

    @Override
    public void render(Game game, Renderer renderer) {
        Canvas canvas = renderer.getCanvas();

        // Rocket's position
        Vector2 rocketPosition = getPosition();

        // Remember the current canvas state, because the entire canvas is rotated by canvas.rotate
        canvas.save();

        // Rotate the canvas around the centre of the rocket
        double angle = physicsState.velocity.direction();
        canvas.rotate((float) angle, (int) rocketPosition.getX(), (int) rocketPosition.getY());

        // Leave the specifics of rendering to whatever state the rocket is currently in
        state.render(game, renderer);

        // Undo the rotation of the entire canvas, excluding the rocket, so any future objects drawn
        // are not rotated with the rocket
        canvas.restore();

        super.render(game, renderer);
    }

    @Override
    public Rect getCollisionRectangle() {
        if (state instanceof RocketDyingState) {
            // Prevent collisions with an inactive rocket
            return null;
        }

        return collisionRectangle;
    }

    @Override
    public void collidedWith(PhysicsEntity otherEntity) {
        if (otherEntity instanceof Monkey) {
            die();
        }
    }


    /**
     * The current state of the rocket (either RocketFlyingState or RocketDyingState)
     * Different states have different update and render behaviors
     */
    private RocketState state;

    private abstract class RocketState {
        RocketState() {
            init();
        }

        public void init() {
        }

        public abstract void update(Game game, long elapsed);
        public abstract void render(Game game, Renderer renderer);
    }

    @Override
    public void onDetached() {
        super.onDetached();

        if (flyingSoundPlayback != null) {
            flyingSoundPlayback.stop();
        }
    }

    /**
     * Controls the behavior of the rocket while it is in-flight and targeting the player
     */
    private class RocketFlyingState extends RocketState {

        // Milliseconds of flight time remaining
        private long timeTotal = 5000;
        private long timeRemaining = timeTotal;

        @Override
        public void init() {
            timeRemaining = 2000 + MathUtilities.randomInRange(0, 3000);
            flyingSoundPlayback = flyingSound.play();
        }

        @Override
        public void update(Game game, long elapsed) {
            // Reduce the lifespan of the rocket
            timeRemaining -= elapsed;

            PhysicsEntity target = game.getMonkeyEntity();

            if (timeRemaining <= 0 || getPosition().getY() < target.getPosition().getY()) {
                // Flying time is up!
                // Transition to the dying state
                die();
                return;
            }

            // The animation tracker will handle updating which frame is currently displayed
            animationTracker.advance(elapsed);

            updateCourse(target, elapsed);
        }

        private Vector2.Mutable predicatedPosition = new Vector2.Mutable();

        /**
         * Recalculates the course of the rocket to aim at the given target object
         *
         * @param target  The (potentially moving) target to aim for
         * @param elapsed Milliseconds elapsed since last course update
         *
         * TODO: Maybe make this a separate algorithm that isn't specific to the Rocket class?
         */
        protected void updateCourse(PhysicsEntity target, long elapsed) {
            // Estimate how far the target will move before the next update
            target.getPhysicsState().predictPosition(elapsed, Vector2.X_ONLY, predicatedPosition);

            // Use the predicted position as the target when updating the course
            updateCourse(predicatedPosition, elapsed);
        }

        private Vector2.Mutable desiredCourse = new Vector2.Mutable();

        /**
         * Recalculates the course of the rocket to aim at the given point
         *
         * @param targetPosition The position to aim for
         * @param elapsed Milliseconds elapsed since last course update
         *
         * TODO: Maybe make this a separate algorithm that isn't specific to the Rocket class?
         */
        protected void updateCourse(Vector2 targetPosition, long elapsed) {
            // Rocket position:
            Vector2 rocketPosition = getPosition();

            // Current course of the rocket
            Vector2 oldCourse = physicsState.velocity;

            // Calculate direction of the monkey's position from the rocket's position
            // This is the new (desired) course of the rocket to aim to hit the target
            desiredCourse.set(
                targetPosition.getX() - rocketPosition.getX(),
                targetPosition.getY() - rocketPosition.getY()
            );

            // Rocket can go no slower than the minimum speed
            double desiredSpeed = desiredCourse.magnitude();
            double rocketSpeed = Math.min(Math.max(desiredSpeed, minimumSpeed), maximumSpeed);

            // The angle the rocket is currently heading in
            double currentAngle = oldCourse.direction();

            // The optimal angle that would take the rocket directly to the target
            double desiredAngle = desiredCourse.direction();

            // The angle the rocket wants to turn through
            double desiredTurnAngle = MathUtilities.normalizeAngle(desiredAngle - currentAngle);
            double desiredTurnDirection = Math.signum(desiredTurnAngle);
            double desiredTurnSize = Math.abs(desiredTurnAngle);

            // Speed of desired turn (degrees per millisecond)
            double desiredTurnRate = desiredTurnSize / elapsed;

            // Limit the agility (turning speed) of the rocket
            double rocketTurnRate = Math.min(desiredTurnRate, maximumTurnRate);

            // Calculate new turn after the limit has been applied
            double rocketTurnSize = rocketTurnRate * elapsed;
            double rocketTurnAngle = rocketTurnSize * desiredTurnDirection;
            double rocketAngle = currentAngle + rocketTurnAngle;

            // The physics entity handles all of the movement in the game for us.
            // Velocity is a speed and a direction
            // This is the speed of the rocket and the direction that it is travelling
            physicsState.setVelocity(desiredCourse.setMagnitudeAndDirection(rocketSpeed, rocketAngle));
        }

        @Override
        public void render(Game game, Renderer renderer) {
            // Draw the current animation frame at the current position of the rocket
            animationTracker.draw(renderer.getCanvas(), getRectangle());
        }

    }

    /**
     * Actions that happen when the rocket is dying
     */
    private class RocketDyingState extends RocketFlyingState {

        private Vector2 target;

        @Override
        public void init() {
            dyingSound.play();
            maximumTurnRate = 360. / 1000;
        }

        @Override
        public void update(Game game, long elapsed) {
            if (target == null) {
                // The rocket will fall down towards the bottom of the screen, but ensuring it is not
                // off to the left or right side
                target = game.getRenderer().getScaledPosition(
                    new Vector2(
                        Math.min(Math.max(0.3, getPosition().getX()), 0.7),
                        2
                    )
                );
            }

            // Ensure that the rocket can fall off the screen faster than the monkey is falling
            minimumSpeed = game.getMonkeyEntity().getPhysicsState().velocity.getY() * 1.5;
            maximumSpeed = Double.POSITIVE_INFINITY;

            // Just use the targeting algorithm to drop to this position
            updateCourse(target, elapsed);
        }

        @Override
        public void render(Game game, Renderer renderer) {
            // In the dying state, draw the image of the rocket with no flame trail
            renderer.getCanvas().drawBitmap(dyingImage, null, getRectangle(), null);
        }

    }

}
