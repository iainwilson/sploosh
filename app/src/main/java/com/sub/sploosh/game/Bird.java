package com.sub.sploosh.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.sub.sploosh.engine.Animation;
import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.audio.Sound;
import com.sub.sploosh.engine.math.MathUtilities;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

public class Bird extends PhysicsEntity {

    private final int SPAWN_LEFT = 0;
    private final int SPAWN_RIGHT = 1;

    private final int MOVE_RIGHT = 1;
    private final int MOVE_LEFT = -1;

    private int direction;
    private final double speed;

    private static Bitmap sprite;
    private static Animation animation;

    private static final float frameX = 182;
    private static final float frameY = 170;

    // Custom hitbox
    private static final Rect frameHitbox = new Rect(61, 61, 146, 141);
    private final Rect scaledHitbox = new Rect(), hitbox = new Rect();

    private static Sound squawkSound;//James

    // preloads the bird's assets
    public static void preLoad(Game game) {
        sprite = game.getAssetManager().loadAndAddAndGetBitmap("bird", "redBird.png");
        animation = new Animation(sprite, new Vector2(frameX, frameY), 50);
        squawkSound = game.getAssetManager().loadAndAddAndGetSound("bird-sound", "audio/bird-sound.mp3");//James
    }

    private final Animation.Tracker animationTracker = animation.createTracker();

    public Bird(Game game) {
        super(game.getPhysicsWorld());

        int spawnPosition;

        Vector2 gameSize = game.getRenderer().getSize();

        // 50:50 chance for either spawn location
        if (MathUtilities.randomInRange(1, 100) % 2 == 0) {
            // Start on left with positive direction on x-axis (move right)
            direction = MOVE_RIGHT;
            spawnPosition = SPAWN_LEFT;
            squawkSound.play();//James
        }
        else {
            // Start on right with negative direction on x-axis (move left)
            direction = MOVE_LEFT;
            spawnPosition = SPAWN_RIGHT;
            squawkSound.play();//James
        }

        // Default speed is moving the width of the game in one second
        this.speed = gameSize.getX();

        // Bird width is set to a quarter of the screen width and the height is scaled accordingly
        setSize(game.getRenderer().getScaledSize(animation.getFrameSize(), 0.25, null));

        // Bird spawns below the screen, on the left or right of the screen depending on the value of spawnPosition
        this.physicsState.setPosition(gameSize.getX() * spawnPosition, gameSize.getY() + getSize().getY() / 2);
        this.physicsState.setVelocity(speed * direction, 0);

        double scale = getSize().getX() / frameX;

        // Calculates the position of the hitbox taking into account the size the bird is on screen
        scaledHitbox.set(
            (int) (frameHitbox.left * scale),
            (int) (frameHitbox.top * scale),
            (int) (frameHitbox.right * scale),
            (int) (frameHitbox.bottom * scale)
        );

    }

    @Override
    public void update(Game game, long elapsed) {
        animationTracker.advance(elapsed);

        Rect gameBounds = game.getRenderer().getBounds();
        Vector2 position = getPosition();

        // Caclulcates where the hitbox is after the bird's position has changed
        hitbox.set(scaledHitbox);
        hitbox.offset(getRectangle().left, getRectangle().top);

        // When the bird reaches either side of the screen, it will change direction
        if (position.getX() >= gameBounds.right) {
            // Gone off the right
            direction = MOVE_LEFT;
        }
        else if (position.getX() <= gameBounds.left) {
            // Gone off the left
            direction = MOVE_RIGHT;
        }

        // reduce the speed of the bird as it nears the edge of the screen
        double xPosition = this.getPosition().getX() / gameBounds.width();
        double phase = xPosition * Math.PI;
        double velocityFactor = 0.25 + 0.75 * Math.sin(phase);

        double velocityX = speed * direction * velocityFactor;
        double velocityY = 0.6 * game.getMonkeyEntity().getPhysicsState().velocity.getY();

        double minSpeedY = gameBounds.height() / 5;

        // When the monkey is falling slowly, prevent the bird from moving downwards
        if (Math.abs(velocityY) < minSpeedY) {
            velocityY = -minSpeedY;
        }

        physicsState.setVelocity(velocityX, velocityY);

        super.update(game, elapsed);
    }

    @Override
    public void render(Game game, Renderer renderer) {
        Canvas canvas = renderer.getCanvas();
        canvas.save();

        if (physicsState.velocity.getX() < 0) {
            // Bird is travelling left and needs flipped
            Vector2 position = getPosition();
            canvas.scale(-1, 1, (float) position.getX(), (float) position.getY());
        }

        animationTracker.draw(renderer.getCanvas(), getRectangle());
        super.render(game, renderer);

        canvas.restore();
    }

    @Override
    public Rect getCollisionRectangle() {
        return hitbox;
    }

}
