package com.sub.sploosh.game;

import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.RenderableEntity;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.MathUtilities;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsState;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;


/**
 * Joint effort by Iain, Fiona and Liam on 02/03/2016.
 */

public class Bolt extends RenderableEntity {

    private final Paint boltPaint = new Paint();
    private final Paint blurPaint = new Paint();

    {
        blurPaint.setStrokeWidth(4);
        blurPaint.setColor(Color.YELLOW);
        blurPaint.setMaskFilter(new BlurMaskFilter(10, BlurMaskFilter.Blur.NORMAL));
    }

    private static class Line {

        public Vector2 start, end;

        public Line(Vector2 start, Vector2 end) {
            this.start = start;
            this.end = end;
        }

    }

    private final List<Line> lines;

    public Bolt(Vector2 start, Vector2 end) {
        this.lines = generate(
                new Vector2(start.getX(), start.getY()),
                new Vector2(end.getX(), end.getY())
        );
    }

    // Constants to control the generation algorithm

    // Max amount to "bend" a fork of lightning at its midpoint
    static final int maxOffset = 100;

    // How many iterations of the algorithm to run
    static final int maxIterations = 5;

    // Largest angle a fork can deviate from the line segment it branches from
    static final int maxForkAngle = 90;

    // How long forks are compared to the line segment they branch from (e.g. 0.5 = 50%)
    static final double forkFactor = 0.5;

    /**
     * @author Liam and Iain
     * @param start The source of the lightning
     * @param end   The target of the lightning
     * @return A list of Lines which form a lightning-like path
     *
     * Implementation of the algorithm described by http://drilian.com/2009/02/25/lightning-bolts/
     * 
     */
    public static List<Line> generate(Vector2 start, Vector2 end) {
        LinkedList<Line> lines = new LinkedList<>();
        lines.add(new Line(start, end));

        double offset = maxOffset;

        for (int iteration = 0; iteration < maxIterations; iteration++) {
            ListIterator<Line> lineIterator = lines.listIterator();

            while (lineIterator.hasNext()) {
                // Remove the current line segment from the list
                Line line = lineIterator.next();
                lineIterator.remove();

                // Find the midpoint of the line
                Vector2 midPoint = line.start.plus(line.end).times(0.5);

                // Random number (-1 to 1) to offset the midpoint by
                double offsetAmount = (MathUtilities.randomInRange(-1000, 1000) / 1000.) * offset;

                // Shift the midpoint of the line in or out along a line perpendicular to the original
                midPoint = midPoint.plus(line.end.minus(line.start).normalized().perpendicular().times(offsetAmount));

                // Replace the old line with two new lines,
                // one from the starting point of the old line to the new midpoint
                lineIterator.add(new Line(line.start, midPoint));
                // and one from the new midpoint to the end point of the old line
                lineIterator.add(new Line(midPoint, line.end));

                // Add a third fork
                Vector2 firstHalf = midPoint.minus(line.start);
                double forkLength = firstHalf.magnitude() * forkFactor;

                // Angle that the fork deviates from the existing line
                double forkAngle = MathUtilities.randomInRange(-maxForkAngle, +maxForkAngle);
                double forkDirection = firstHalf.direction() + forkAngle;

                Vector2 fork = midPoint.plus(Vector2.fromMagnitudeAndDirection(forkLength, forkDirection));
                lineIterator.add(new Line(midPoint, fork));
            }

            // Size used for the midpoint offset halves each iteration
            offset /= 2;
        }

        return lines;
    }

    private long timeLimit = 2000;
    private long timeElapsed = 0;
    private Vector2 killPoint = new Vector2(0, Double.POSITIVE_INFINITY);

    public Vector2 getKillPoint() {
        return killPoint;
    }

    @Override
    public void update(Game game, long elapsed) {
        timeElapsed += elapsed;

        super.update(game, elapsed);

    }

    /**
     * @author Fiona and Iain
     */
    @Override
    public void render(Game game, Renderer renderer) {
        Canvas canvas = renderer.getCanvas();
        boltPaint.setColor(Color.YELLOW);
        boltPaint.setStrokeWidth(2);


//        BlurMaskFilter blurMaskFilter = new BlurMaskFilter(10, BlurMaskFilter.Blur.OUTER);
//        blur.setColor(Color.WHITE);
//        blur.setMaskFilter(blurMaskFilter);
//        blur.setStrokeWidth(5);

//Creating a glow around the lightning and give the illusion it is flickering
        int yellow = MathUtilities.randomInRange(220, 255);
        int alpha = MathUtilities.randomInRange(192, 255);
        boltPaint.setARGB(alpha, yellow, yellow, yellow);
        boltPaint.setAntiAlias(true);

        // Between 0..1
        double amount = (double) timeElapsed / timeLimit;
        int n = (int) Math.min(lines.size() * amount, lines.size());

        for (Line line : lines.subList(0, n)) {

            //draw blur
            canvas.drawLine((float) line.start.getX(), (float) line.start.getY(),
                    (float) line.end.getX(), (float) line.end.getY(), blurPaint);
            //draw lines
            canvas.drawLine((float) line.start.getX(), (float) line.start.getY(),
                    (float) line.end.getX(), (float) line.end.getY(), boltPaint);

            //testing out the end point of the lightning is what kills the monkey
            if (line.end.getY() < killPoint.getY()) {
                killPoint = line.end;
            }

        }

        if (Renderer.isDebugging(Renderer.DebugFlag.Hitboxes)) {
            boltPaint.setColor(Color.GREEN);
            canvas.drawCircle((float) killPoint.getX(), (float) killPoint.getY(), 5, boltPaint);
        }
    }


}

