package com.sub.sploosh.game;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.sub.sploosh.Application;

/**
 * @author Iain
 *
 * Provides a way for a named preference to have a default value and notify a listener when the preference is changed
 */
public class Preference {

    public interface ChangeListener {
        void onChanged(boolean value);
    }

    private final SharedPreferences preferences;
    private final String name;
    private final boolean defaultValue;
    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceListener;
    private ChangeListener changeListener;

    public Preference(String name, boolean defaultValue) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(Application.getContext());
        this.name = name;
        this.defaultValue = defaultValue;

        this.preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (Preference.this.name.equals(key) && changeListener != null) {
                    changeListener.onChanged(get());
                }
            }
        };
    }

    public void set(boolean value) {
        preferences.edit().putBoolean(name, value).commit();
        update();
    }

    /**
     * Associate a listener with this preference which will be notified when a change occurs
     */
    public void setListener(ChangeListener changeListener) {
        this.changeListener = changeListener;
        update();
    }

    /**
     * Notify listeners of the current value for this preference
     */
    public void update() {
        System.out.printf("Current value for '%s' is %s%n", name, get() ? "enabled" : "disabled");
        this.changeListener.onChanged(get());
    }

    public boolean get() {
        return preferences.getBoolean(name, defaultValue);
    }

    public void enable() {
        set(true);
    }

    public void disable() {
        set(false);
    }

    public static final Preference tiltControl = new Preference("Tilt Preferences", false);
    public static final Preference frameRate   = new Preference("FPS Display Preferences", false);
    public static final Preference debugRenderer = new Preference("debug-bounds", false);
    public static final Preference debugHitboxes = new Preference("debug-hitboxes", false);
    public static final Preference debugCollisions = new Preference("debug-collisions", false);
    public static final Preference godMode = new Preference("god-mode", false);

    private static final Preference[] allPreferences = new Preference[] {
        tiltControl,
        frameRate,
        debugRenderer,
        debugHitboxes,
        debugCollisions,
        godMode
    };

    /**
     * Notifies listeners of the current value of all preferences
     */
    public static void updateAll() {
        for (Preference preference : allPreferences) {
            preference.update();
        }
    }

}
