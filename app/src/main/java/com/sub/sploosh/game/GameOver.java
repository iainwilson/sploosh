package com.sub.sploosh.game;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.RenderableEntity;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.audio.Sound;
import com.sub.sploosh.engine.math.Vector2;

/**
 * Value interpolator for animations
 */
class Interpolator<T> {
    final Algorithm<T> algorithm;
    final Easing easing;
    final T start, end;
    final long duration;
    long currentTime;

    public interface Algorithm<T> {
        T interpolate(T start, T end, double fraction);
    };

    public interface Easing {
        double apply(double fraction);
    }

    // Formula from http://robertpenner.com/easing/
    public static final Easing ELASTIC_BOUNCE_OUT = new Easing() {
        @Override
        public double apply(double t) {
            if ((t /= 1.) < (1 / 2.75)) {
                return (7.5625 * t * t);
            } else if (t < (2 / 2.75)) {
                return (7.5625 * (t -= (1.5 / 2.75)) * t + .75);
            } else if (t < (2.5 / 2.75)) {
                return (7.5625 * (t -= (2.25 / 2.75)) * t + .9375);
            } else {
                return (7.5625 * (t -= (2.625 / 2.75)) * t + .984375);
            }
        }
    };

    public static final Algorithm<Double> DOUBLE_ALGORITHM = new Algorithm<Double>() {
        @Override
        public Double interpolate(Double start, Double end, double fraction) {
            return start + fraction * (end - start);
        }
    };

    public static final Algorithm<Vector2> VECTOR2_ALGORITHM = new Algorithm<Vector2>() {
        @Override
        public Vector2 interpolate(Vector2 start, Vector2 end, double fraction) {
            return new Vector2(
                DOUBLE_ALGORITHM.interpolate(start.getX(), end.getX(), fraction),
                DOUBLE_ALGORITHM.interpolate(start.getY(), end.getY(), fraction)
            );
        }
    };

    /**
     * @param start Initial value
     * @param end Final value
     * @param duration Animation duration in milliseconds
     * @param algorithm Algorithm used to produce interpolated values
     * @param easing Easing algorithm controlling in / out transitions
     */
    public Interpolator(T start, T end, long duration, Algorithm<T> algorithm, Easing easing) {
        this.start = start;
        this.end = end;
        this.duration = duration;
        this.currentTime = 0;
        this.algorithm = algorithm;

        if (easing == null) {
            this.easing = new Easing() {
                @Override
                public double apply(double fraction) {
                    return fraction;
                }
            };
        }
        else {
            this.easing = easing;
        }
    }

    /**
     * Progress the animation
     * @param elapsed Time passed (milliseconds)
     */
    public void advance(long elapsed) {
        this.currentTime = Math.min(duration, currentTime + elapsed);
    }

    /**
     * @return Value calculated by current time
     */
    public T value() {
        return algorithm.interpolate(start, end, easing.apply((double) currentTime / duration));
    }

}

/**
 * Created by Iain on 24/02/2016.
 */
public class GameOver extends RenderableEntity {

    private static class AnimatableEntity extends RenderableEntity {
        private final Interpolator<Vector2> positionAnimation;

        public AnimatableEntity(Bitmap bitmap, Vector2 size, Interpolator<Vector2> positionAnimation) {
            this.bitmap = bitmap;

            setSize(size);
            setPosition(positionAnimation.value());
            this.positionAnimation = positionAnimation;
        }

        @Override
        public void update(Game game, long elapsed) {
            positionAnimation.advance(elapsed);
        }

        @Override
        public void render(Game game, Renderer renderer) {
            setPosition(game.getRenderer().getScaledPosition(positionAnimation.value()));

            super.render(game, renderer);
        }
    }

    private final AnimatableEntity ground, deadMonkey, tombstone, flowers, mourner;
    private static Bitmap groundBitmap, deadMonkeyBitmap, tombstoneBitmap, flowersBitmap, mournerBitmap;
    private static Sound thudSound;

    public static void preLoad(Game game) {
        groundBitmap = game.getAssetManager().loadAndAddAndGetBitmap("ground", "ground.png");
        deadMonkeyBitmap = game.getAssetManager().loadAndAddAndGetBitmap("dead-monkey", "monkey_dead.png");
        tombstoneBitmap = game.getAssetManager().loadAndAddAndGetBitmap("tombstone", "tombstone.png");
        flowersBitmap = game.getAssetManager().loadAndAddAndGetBitmap("flowers", "flowers.png");
        thudSound = game.getAssetManager().loadAndAddAndGetSound("thud", "audio/thud.wav");

    }

    public GameOver(Game game) {
        Vector2 gameSize = game.getRenderer().getSize();

        // Set up animation paths and calculate positioning independent of the game size

        // Position the ground to be at the bottom of the screen and the full width
        Vector2 groundSize = game.getRenderer().getScaledSize(Renderer.getBitmapSize
                (groundBitmap), 1.05, null);
        Vector2 groundPosition = new Vector2(0.5, 1.05 - (groundSize.getY() / gameSize.getY()) / 2);

        this.ground = new AnimatableEntity(
            groundBitmap,
            groundSize,
            new Interpolator<Vector2>(new Vector2(0.5, 1.5), groundPosition, 1000, Interpolator.VECTOR2_ALGORITHM, null)
        );

        this.deadMonkey = new AnimatableEntity(
                deadMonkeyBitmap,
                game.getRenderer().getScaledSize(Renderer.getBitmapSize(deadMonkeyBitmap), null, (groundSize.getY() / gameSize.getY() * (2. / 3.))),
                // Position the dead monkey in the centre of the ground
                new Interpolator<Vector2>(new Vector2(0.5, 1.5), new Vector2(0.5, groundPosition.getY()), 1000, Interpolator.VECTOR2_ALGORITHM, null)
        );

        // Position the tombstone on top of the ground
        double tombstoneHeight = 1. / 6.;
        Vector2 tombstonePosition = new Vector2(0.5, groundPosition.getY() - ((groundSize.getY() / gameSize.getY()) / 2) - (tombstoneHeight / 2) + (1. / 100.));
        Vector2 tombstoneSize = game.getRenderer().getScaledSize(Renderer.getBitmapSize
                (tombstoneBitmap), null, tombstoneHeight);

        this.tombstone = new AnimatableEntity(
                tombstoneBitmap,
                tombstoneSize,
                new Interpolator<Vector2>(new Vector2(0.5, -0.5), tombstonePosition, 1000, Interpolator.VECTOR2_ALGORITHM, Interpolator.ELASTIC_BOUNCE_OUT)
        ) {
            private final Paint textPaint = new Paint();

            {
                textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                textPaint.setTextSize(30);
                textPaint.setTextAlign(Paint.Align.CENTER);
            }

            @Override
            public void render(Game game, Renderer renderer) {
                super.render(game, renderer);

                renderer.getCanvas().drawText(
                    String.format("%d", game.getScoreKeeper().getScore()),
                    (float) (getPosition().getX() + getSize().getX() * 0.05),
                    (float)  getPosition().getY(),
                    textPaint
                );
            }
        };

        this.flowers = new AnimatableEntity(
                flowersBitmap,
                game.getRenderer().getScaledSize(Renderer.getBitmapSize(flowersBitmap), null, 1. / 6.),
                new Interpolator<Vector2>(new Vector2(0.25, 1.5), new Vector2(0.25, tombstonePosition.getY()), 1000, Interpolator.VECTOR2_ALGORITHM, null)
        );

        Bitmap mournerBitmap = game.getAssetManager().loadAndAddAndGetBitmap("mourner", "mourner.png");

        this.mourner = new AnimatableEntity(
                mournerBitmap,
                game.getRenderer().getScaledSize(Renderer.getBitmapSize(mournerBitmap), null, 1. / 4.),
                new Interpolator<Vector2>(new Vector2(1.5, 6. / 10.), new Vector2(0.75, 6. / 10.), 1000, Interpolator.VECTOR2_ALGORITHM, null)
        );
    }

    private boolean triggeredGameOver = false;
    private long totalTimeElapsed = 0;
    private Sound.Playback thud;

    @Override
    public void update(Game game, long elapsed) {
        ground.update(game, elapsed);
        deadMonkey.update(game, elapsed);

        // Sequencing of individual animations
        if (totalTimeElapsed > 1000) {
            tombstone.update(game, elapsed);
        }

        if (totalTimeElapsed > 1250) {
            if (thud == null) {
                thud = thudSound.play();
            }
        }

        if (totalTimeElapsed > 2000) {
            flowers.update(game, elapsed);
        }

        if (totalTimeElapsed > 3000) {
            mourner.update(game, elapsed);
        }

        if (totalTimeElapsed > 2000 && !triggeredGameOver) {
            game.triggerEvent(Game.Event.GameOver);
            triggeredGameOver = true;
        }

        totalTimeElapsed += elapsed;
    }

    @Override
    public void render(Game game, Renderer renderer) {
        tombstone.render(game, renderer);
        flowers.render(game, renderer);
        ground.render(game, renderer);
//        mourner.render(game, renderer);
        deadMonkey.render(game, renderer);
    }


}
