package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;

/**
 * Created by Fiona Mallett and Liam Mullen on 02/03/2016.
 */
public class ThunderCloudSpawner extends ObstacleSpawner<ThunderCloud> {

    private static final int MAXIMUM = 1;
    private long timePassed, timer;

    public ThunderCloudSpawner(Game game) {
        super(game);
    }


    @Override
    protected ThunderCloud spawn() {

        return new ThunderCloud(game);
    }

    @Override
    protected boolean shouldRemove(ThunderCloud entity) {

        //If monkey is dead
        if (!game.getMonkeyEntity().isAlive()) {
            return true;
        }

        //only allow the cloud to stay on the screen for 5 seconds
        else if (timer >= 5000) {
            timer = 0;
            return true;
        }

        //if off the screen
        else return entity.getPosition().getY() < 0;
    }

    @Override
    protected int getMaximumEntities() {
        return MAXIMUM;
    }

    @Override
    public void update(Game game, long elapsed) {
        //timePassed used for spawning
        timePassed += elapsed;
        //timer used for keeping the thundercloud on screen for a certain amount of time
        timer += elapsed;
        super.update(game, elapsed);
    }

}
