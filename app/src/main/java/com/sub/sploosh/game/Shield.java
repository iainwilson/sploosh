package com.sub.sploosh.game;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

/**
 * Created by Fiona Mallett on 24/02/2016.
 */
public class Shield extends PhysicsEntity {

    public Shield(Game game) {
        super(game.getPhysicsWorld());
        Vector2 gameSize = game.getRenderer().getSize();
        this.bitmap = game.getAssetManager().loadAndAddAndGetBitmap("shield", "shield.png");
        setSize(game.getRenderer().getScaledSize(Renderer.getBitmapSize(bitmap), 0.1, null));
        //spawn the shield
        this.physicsState.setPosition(gameSize.getX() / 2, gameSize.getY());
    }

    @Override
    public void render(Game game, Renderer renderer) {
        super.render(game, renderer);
    }

    @Override
    public void update(Game game, long elapsed) {
        super.update(game, elapsed);
    }


}
