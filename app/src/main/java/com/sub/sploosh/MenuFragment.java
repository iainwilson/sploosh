package com.sub.sploosh;

import android.annotation.SuppressLint;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MenuFragment extends ListFragment {

    public class DemoDetails {

        // ////////////////////////////////////////////////////////////////////////
        // Properties
        // ////////////////////////////////////////////////////////////////////////

        /**
         * Descriptive name of the demo
         */
        private String mName;

        /**
         * Demo class name
         */
        private String mClassName;

        /**
         * Location of the demo class relative to demo manager
         */
        private String mClassLocation;

        // ////////////////////////////////////////////////////////////////////////
        // Constructors
        // ////////////////////////////////////////////////////////////////////////

        /**
         * Create overview details of a demo
         *
         * @param name
         *            Descriptive name of the demo
         * @param className
         *            Class name for the demo
         * @param classLocation
         *            Location of the demo relative to the manager
         */
        public DemoDetails(String name, String className, String classLocation) {
            mName = name;
            mClassName = className;
            mClassLocation = classLocation;
        }

        // ////////////////////////////////////////////////////////////////////////
        // Methods
        // ////////////////////////////////////////////////////////////////////////

        /**
         * Get the descriptive name of this demo
         *
         * @return Descriptive name of this demo
         */
        public String getName() {
            return mName;
        }

        /**
         * Get the class name of this demo
         *
         * @return Class name of this demo
         */
        public String getClassName() {
            return mClassName;
        }

        /**
         * Get the location of the demo class relative to demo manager
         *
         * @return Location of the demo class relative to demo manager
         */
        public String getClassLocation() {
            return mClassLocation;
        }
    }

    public class DemoManager {

        // ////////////////////////////////////////////////////////////////////////
        // Properties
        // ////////////////////////////////////////////////////////////////////////

        /**
         * List of available demos
         */
        private ArrayList<DemoDetails> mDemoDetails;

        /**
         * Context for the demo manager (from which the demos will be run)
         */
        private Context mContext;

        // ////////////////////////////////////////////////////////////////////////
        // Constructors
        // ////////////////////////////////////////////////////////////////////////

        /**
         * Create a new demo manager
         *
         * @param context
         *            Context from which demos will be run
         */
        public DemoManager(Context context) {
            mContext = context;
            mDemoDetails = new ArrayList<DemoDetails>();
        }

        // ////////////////////////////////////////////////////////////////////////
        // Methods
        // ////////////////////////////////////////////////////////////////////////

        /**
         * Add the set of demo details to the manager
         *
         * @param demoDetail
         *            Demo details
         */
        public void add(DemoDetails demoDetail) {
            mDemoDetails.add(demoDetail);
        }

        /**
         * Return all available demos
         *
         * @return List of all available demos
         */
        public ArrayList<DemoDetails> getDemoDetails() {
            return mDemoDetails;
        }

        /**
         * Return the context from which the demos will be run
         *
         * @return Context from which the demos will be run
         */
        public Context getContext() {
            return mContext;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Fragment#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup the available demos
        defineAndAddDemos();

        // Add the demos to the list to be displayed
        setListAdapter(new DemoDetailsAdapter(getActivity(), 0,
                mDemoManager.getDemoDetails()));
    }

    /**
     * Adapter class for displaying details of each demo
     */
    private class DemoDetailsAdapter extends ArrayAdapter<DemoDetails> {

        /**
         * Create a new demo adapter
         *
         * @param context
         *            Context for the adapter
         * @param resource
         *            Resource ID
         * @param objects
         *            List of demos
         */
        public DemoDetailsAdapter(Context context, int resource,
                                  List<DemoDetails> objects) {
            super(context, resource, objects);
        }

        /*
         * (non-Javadoc)
         *
         * @see android.widget.ArrayAdapter#getView(int, android.view.View,
         * android.view.ViewGroup)
         */
        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // Inflate a simple list item view if needed
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        android.R.layout.simple_list_item_1, null);
            }

            // Add in details for the demo at this position
            DemoDetails demoDetails = getItem(position);
            ((TextView) convertView).setText(demoDetails.getName());

            return convertView;
        }
    }

    // ////////////////////////////////////////////////////////////////////////
    // Available demos
    // ////////////////////////////////////////////////////////////////////////

    /**
     * Manager responsible for holding details of all available demos
     */
    private DemoManager mDemoManager;

    /**
     * Setup the available demos
     */
    private void defineAndAddDemos() {

        mDemoManager = new DemoManager(getActivity());

        mDemoManager.add(new DemoDetails("Play Sploosh", "GameActivity", "demos"));
        mDemoManager.add(new DemoDetails("TransformsActivity",  "TransformsActivity", "demos"));
        mDemoManager.add(new DemoDetails("TouchInputActivity",  "TouchInput", "demos"));
        mDemoManager.add(new DemoDetails("RibbonActivity",  "Ribbon", "demos"));
        mDemoManager.add(new DemoDetails("MovingMonkey",  "MovingMonkey", "demos"));
        mDemoManager.add(new DemoDetails("AccelerationDemo",  "AccDemo", "demos"));


    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.ListFragment#onListItemClick(android.widget.ListView,
     * android.view.View, int, long)
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        // Get details of the selected demo
        DemoDetails demoDetails = mDemoManager.getDemoDetails().get(position);

        try {
            String className = mDemoManager.getContext()
                    .getPackageName()
                    + "."
                    + demoDetails.getClassLocation()
                    + "." + demoDetails.getClassName();

            // Load the demo class (which will be a Fragment subclass)
            Class<?> demoClass = Class.forName(className);

            // Introduce the new fragment
            Intent intent = new Intent(getActivity(), demoClass);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            Log.e("Error", e.toString());
        }
    }
}
