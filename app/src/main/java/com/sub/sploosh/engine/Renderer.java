package com.sub.sploosh.engine;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.sub.sploosh.engine.math.Vector2;

public abstract class Renderer {

    public static final double WIDTH = 480;
    public static final double HEIGHT = 856;
    public static final Vector2 CENTER = Vector2.HALF;

    /**
     * Call to indicate to the renderer that a render is about to start.
     *
     *
     * @return
     * true - The underlying canvas is ready for drawing. getCanvas() will not return null
     * false - The underlying canvas is not ready. getCanvas() will return null
     */
    public abstract boolean beginRender();

    /**
     * Call to indicate to the renderer that rendering of the current frame is complete and should
     * be update onscreen. getCanvas() will return null after calling.
     */
    public abstract void endRender();

    /**
     * Returns the canvas for drawing the current frame on, if currently rendering a frame
     */
    public abstract Canvas getCanvas();

    /**
     * Returns the size of the game in game units
     */
    public abstract Vector2 getSize();

    /**
     * Returns the bounds of the renderable area in game units
     *
     * The origin might not be at (0, 0) if the canvas has been scaled
     * to fit the device's screen while maintaining aspect ratio
     */
    public abstract Rect getBounds();

    /**
     *
     * @param bitmap - Bitmap image
     * @return x and y dimensions of the image
     */
    public static Vector2 getBitmapSize(Bitmap bitmap) {
        return new Vector2(bitmap.getWidth(), bitmap.getHeight());
    }

    public static Rect getBitmapRect(Bitmap bitmap) {
        return Vector2.round(Vector2.sizeAtOrigin(getBitmapSize(bitmap)));
    }

    /**
     *
     * Scales the size of (e.g. a bitmap) to a given fraction of the total game size
     * so it will render the same apparent size on any device screen size.
     *
     * Either width or height can be null to keep the aspect ratio of the graphic, but not both
     *
     * @param size Original size
     * @param scaleX Fraction of the width of the game (e.g. 0.25 is 25% of the width of the game)
     * @param scaleY Fraction of the height of the game (e.g. 0.25 is 25% of the height of the game)
     *
     * @code getScaledSize(imageSize, )
     */
    public Vector2.Mutable calculateScaledSize(Vector2 size, Double scaleX, Double scaleY, Vector2.Mutable result) {
        Vector2 gameSize = getSize();

        double ratio = size.getX() / size.getY();
        double width = 0, height = 0;

        if (scaleX == null && scaleY == null) {
            throw new IllegalArgumentException("Either width or height must be given");
        }

        if (scaleX != null) {
            width = gameSize.getX() * scaleX;
        }

        if (scaleY != null) {
            height = gameSize.getY() * scaleY;
        }

        if (scaleX == null) {
            width = height * ratio;
        }

        if (scaleY == null) {
            height = width * (1. / ratio);
        }

        return result.set(width, height);
    }

    public Vector2 getScaledSize(Vector2 size, Double scaleX, Double scaleY) {
        return calculateScaledSize(size, scaleX, scaleY, new Vector2.Mutable());
    }

    /**
     * Get a position relative to the size of the game.
     * e.g:
     *  (0.5, 0.5) is the centre of the game,
     *  (1.0, 1.0) is the bottom right corner
     */
    public Vector2 getScaledPosition(Vector2 position) {
        return calculateScaledPosition(position.getX(), position.getY(), new Vector2.Mutable());
    }

    public Vector2 calculateScaledPosition(double x, double y, Vector2.Mutable result) {
        return result.set(
            getBounds().left + getSize().getX() * x,
            getBounds().top  + getSize().getY() * y
        );
    }

    public Vector2 getCenter() {
        return getScaledPosition(CENTER);
    }

    /**
     * Set the paint used to fill the entire canvas before rendering anything
     */
    public abstract void setBackgroundPaint(Paint backgroundPaint);

    /**
     * True if the entity is entirely outside the rendered area
     */
    public boolean isOffScreen(RenderableEntity entity) {
        return !entity.getRectangle().intersect(getBounds());
    }

    public enum DebugFlag {
        Bounds,
        Hitboxes,
        Collisions
    }

    private static int debugFlags;

    public static boolean isDebugging(DebugFlag flag) {
        return (debugFlags & (1 << flag.ordinal())) > 0;
    }

    public static void setDebugging(DebugFlag flag, boolean state) {
        if (state) {
            debugFlags |=  (1 << flag.ordinal());
        }
        else {
            debugFlags &= ~(1 << flag.ordinal());
        }
    }

    public abstract void dispose();

}
