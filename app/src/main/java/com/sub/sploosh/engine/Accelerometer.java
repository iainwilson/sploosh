package com.sub.sploosh.engine;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Liam Mullen on 23/02/2016.
 */
public class Accelerometer implements SensorEventListener, Input {
    SensorManager sm;
    Sensor accelerometer;
    SensorEvent event;

    public Accelerometer( Activity activity) {
        // Creating the accelerometer
        Context context = activity.getApplicationContext();
        sm = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }







    @Override
    public void onSensorChanged(SensorEvent event) {
        this.event = event;
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    //This class returns the value of acceleration for the Monkey
    @Override
    public double getMovement() {
        //If accelerometer is non functional, acceleration is zero


        return calculateMovement(event);


    }

    public static double calculateMovement(SensorEvent event) {
        //If accelerometer is non functional, acceleration is zero
        if (event==null) {
            return 0;
        }
        else{
            // event.values
            return -event.values[0] / 3;
        }
    }
}
