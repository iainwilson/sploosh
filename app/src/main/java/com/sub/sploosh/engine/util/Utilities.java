package com.sub.sploosh.engine.util;

/**
 * Created by iain on 14/03/16.
 */
public class Utilities {

    public static String getClassName(Object object) {
        Class klass = object.getClass();

        while (klass.isAnonymousClass()) {
            klass = klass.getSuperclass();
        }

        return klass.getName();
    }

}
