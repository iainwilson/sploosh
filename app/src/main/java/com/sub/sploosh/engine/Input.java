package com.sub.sploosh.engine;

/**
 * Created by Liam Mullen on 24/02/2016.
 */
public interface Input {
    public double getMovement();

}
