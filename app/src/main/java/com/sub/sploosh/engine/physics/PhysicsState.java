package com.sub.sploosh.engine.physics;

import com.sub.sploosh.engine.math.Vector2;

import java.util.concurrent.TimeUnit;

/**
 * Represents current
 */
public class PhysicsState {

    // Internally physical properties are mutable, for performance
    private final Vector2.Mutable mutableAcceleration = new Vector2.Mutable();
    private final Vector2.Mutable mutableVelocity = new Vector2.Mutable();
    private final Vector2.Mutable mutablePosition = new Vector2.Mutable();
    private final Vector2.Mutable relativeVelocity = new Vector2.Mutable();

    // Only allow immutable access via the public interface
    public final Vector2 acceleration = mutableAcceleration;
    public final Vector2 velocity = mutableVelocity;
    public final Vector2 position = mutablePosition;

    private final PhysicsWorld world;

    // Various forms of setters for the physical properties

    public void setAcceleration(Vector2 acceleration) {
        this.mutableAcceleration.set(acceleration);
    }

    public void setAcceleration(double x, double y) {
        this.mutableAcceleration.set(x, y);
    }

    public void setVelocity(Vector2 velocity) {
        this.mutableVelocity.set(velocity);
    }

    public void setVelocity(double x, double y) {
        this.mutableVelocity.set(x, y);
    }

    public void setPosition(Vector2 position) {
        this.mutablePosition.set(position);
    }

    public void setPosition(double x, double y) {
        this.mutablePosition.set(x, y);
    }

    /**

     * @param world Physics world this state belongs to
     */
    public PhysicsState(PhysicsWorld world) {
        this.world = world;
    }

    /**
     * Advances this state's physics simulation
     * @param elapsed Milliseconds since last update
     */
    public void update(long elapsed) {
        double seconds = elapsed / 1000.f;

        mutableVelocity.set(
                velocity.getX() + acceleration.getX() * seconds,
                velocity.getY() + acceleration.getY() * seconds
        );

        relativeVelocity.set(velocity);
        world.applyReference(relativeVelocity);

        mutablePosition.set(
            position.getX() + relativeVelocity.getX() * seconds,
            position.getY() + relativeVelocity.getY() * seconds
        );
    }

    /**
     *
     * @param timeFromNow Time (in milliseconds) into the future to predict the position
     * @param factor Can be used to cancel out movement on one axis - e.g. Vector2.X_ONLY or Vector2.Y_ONLY, or null to calculate both
     * @param result Vector which is updated to contain the result
     */
    public void predictPosition(long timeFromNow, Vector2 factor, Vector2.Mutable result) {
        double seconds = timeFromNow / 1000.;

        if (factor == null) {
            factor = Vector2.ONE;
        }

        double halfTimeSquared = 0.5 * Math.pow(seconds, 2);

        result.set(
            position.getX() + velocity.getX() * factor.getX() * seconds + acceleration.getX() * halfTimeSquared,
            position.getY() + velocity.getY() * factor.getY() * seconds + acceleration.getY() * halfTimeSquared
        );
    }

}
