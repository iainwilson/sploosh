package com.sub.sploosh.engine.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 * Created by Iain on 24/02/2016.
 */

/**
 * Iterator over nested data structures.
 * Iterates over Iterables which have Iterable elements, which have Iterable elements, and so on...
 *
 * It's Iterables all the way down, until you reach turtles
 */
public class DepthFirstIterator<T extends Iterable<? extends T>> implements Iterator<T> {

    private final Stack<Iterator<T>> iterators = new Stack<>();

    /** Iterator to item that will be removed by the {@link #remove} method */
    private Iterator<T> removeIterator = null;

    public DepthFirstIterator(Iterator<T> iterator) {
        iterators.push(iterator);
    }

    private Iterator<T> currentIterator() {
        if (iterators.empty()) {
            return Collections.emptyIterator();
        }

        return iterators.peek();
    }

    @Override
    public boolean hasNext() {
        while (!iterators.empty() && !currentIterator().hasNext()) {
            iterators.pop();
        }

        return currentIterator().hasNext();
    }

    @Override
    public T next() {
        T next = currentIterator().next();
        this.removeIterator = currentIterator();

        // Descend into nested items if the current item is iterable
        if (next instanceof Iterable) {
            Iterator<T> nextIterator = ((Iterable<T>) next).iterator();
            iterators.push(nextIterator);
        }

        return next;
    }

    @Override
    public void remove() {
        if (removeIterator == null) {
            throw new IllegalStateException();
        }

        removeIterator.remove();
        removeIterator = null;
    }

}
