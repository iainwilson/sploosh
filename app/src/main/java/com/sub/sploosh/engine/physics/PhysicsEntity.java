package com.sub.sploosh.engine.physics;

import android.graphics.Rect;

import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.RenderableEntity;
import com.sub.sploosh.engine.math.Vector2;

import java.util.Collections;
import java.util.Set;

/** An extension of an Entity that has position, velocity and acceleration vectors */
public abstract class PhysicsEntity extends RenderableEntity {

    // Represents the current state of this entity
    protected final PhysicsState physicsState;

    public PhysicsState getPhysicsState() {
        return physicsState;
    }

    // The "world" that this entity exists within
    protected final PhysicsWorld physicsWorld;

    public PhysicsEntity(Game game) {
        this(game.getPhysicsWorld());
    }

    public PhysicsEntity(PhysicsWorld world) {
        this.physicsWorld = world;
        this.physicsState = new PhysicsState(world);
    }

    @Override
    public Vector2 getPosition() {
        return physicsState.position;
    }

    /**
     * @return Area for which collisions on this entity are detected.
     *         Same as the entity's display rectangle by default.
     *         Return null to indicate no collisions should be detected.
     */
    public Rect getCollisionRectangle() {
        return getRectangle();
    }

    /**
     * Called when a collision between this entity and another are detected.
     * Override in subclasses to add collision handling logic
     *
     * @param otherEntity The other entity involved in the collision
     */
    public void collidedWith(PhysicsEntity otherEntity) {
    }

    @Override
    public void update(Game game, long elapsed) {
        this.physicsState.update(elapsed);
        invalidateBounds();
    }


}
