package com.sub.sploosh.engine;

import android.view.MotionEvent;
import android.view.View;

import com.sub.sploosh.engine.math.Vector2;

public class TouchInput implements View.OnTouchListener, Input {
    @Override
    public double getMovement() {
        return speed;
    }

    private final View view;

    public TouchInput(View view) {
        this.view = view;
        view.setOnTouchListener(this);
    }

    private Vector2 initialPosition = null;
    private Vector2 currentPosition = null;
    private double speed = 0;

    /*
     * Position of the touch relative to the game area
     * e.g. (0, 0) is the top left and (1, 1) is the bottom right
     *
     * Null if no touch is active
     */
    public Vector2 getPosition() {
        return currentPosition;
    }

    private Vector2 calculatePosition(MotionEvent event) {
        double width = view.getWidth();

        // Make the x co-ordinate a fraction of the size of the game view
        // i.e. touch at 160 / 320 = 0.5
        double x = event.getX() / width;

        // Clamp value between 0 and 1
        x = Math.max(0, Math.min(x, 1));

        // Return touch position point, ignore Y co-ordinate for now
        return new Vector2(x, 0);
    }

    public static double getSpeed(MotionEvent event, Vector2 viewSize) {
        double width = viewSize.getX();
        double xVal = event.getX() / width;
        xVal = Math.max(0, Math.min(xVal, 1));
        if(xVal<=0.5){
            return  -1;
        }
        else {
            return 1;
        }
    }

    private Vector2.Mutable viewSize = new Vector2.Mutable();

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        viewSize.set(view.getWidth(), view.getHeight());

        switch (event.getAction()) {
            // Events that start or update a touch
            case MotionEvent.ACTION_DOWN:
                this.speed = getSpeed(event, viewSize);
//                this.initialPosition = calculatePosition(event);
//                this.currentPosition = initialPosition;
            case MotionEvent.ACTION_MOVE:
                this.currentPosition = calculatePosition(event);
                break;

            // Events that touch a cancel
            case MotionEvent.ACTION_OUTSIDE:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
//                this.currentPosition = null;
//                this.initialPosition = null;
                this.speed = 0;
                break;
        }

        return true;
    }

}
