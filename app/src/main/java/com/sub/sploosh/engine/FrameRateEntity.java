package com.sub.sploosh.engine;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Entity which calculates and displays the current framerate
 * (the number of frames rendered in the previous windowDuration milliseconds)
 */
public class FrameRateEntity extends RenderableEntity {

    private final Paint paint = new Paint();

    private final long windowDuration = 1000;
    private long windowFrames = 0, windowTime = 0;
    private double frameRate = 0;

    public FrameRateEntity() {
        paint.setColor(Color.WHITE);
        paint.setTextSize(30);
    }

    @Override
    public void update(Game game, long elapsed) {
        if (windowTime >= windowDuration) {
            frameRate = windowFrames / (windowTime / 1000.f);
            windowFrames = 0;
            windowTime -= windowDuration;
        }

        windowFrames++;
        windowTime += elapsed;
    }

    @Override
    public void render(Game game, Renderer renderer) {

            String message = String.format("%.1f FPS", frameRate);
            renderer.getCanvas().drawText(message, 200, 30, paint);

    }
}
