package com.sub.sploosh.engine;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsEntity;

/**
 * An extension of an {@link Entity} which has some on-screen representation and will be requested to
 * render itself each frame (by calling {@link #render(Game, Renderer)})
 */
public abstract class RenderableEntity extends Entity {

    protected Bitmap bitmap;

    private final Vector2.Mutable position = new Vector2.Mutable();
    private final Vector2.Mutable size = new Vector2.Mutable();

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position.set(position);
        invalidateBounds();
    }

    public Vector2 getSize() {
        return size;
    }

    public void setSize(Vector2 size) {
        this.size.set(size);
        invalidateBounds();
    }

    private final Rect bounds = new Rect();

    /**
     * Rectangle occupied by this object on-screen
     */
    public Rect getRectangle() {
        return bounds;
    }

    // Call to update bounds after position or size has changed
    protected void invalidateBounds() {
        // Avoid direct access, because these can be and are overrode in subclasses
        Vector2 position = getPosition();
        Vector2 size = getSize();

        bounds.top = (int) (position.getY() - size.getY() / 2);
        bounds.left = (int) (position.getX() - size.getX() / 2);
        bounds.bottom = (int) (bounds.top + size.getY());
        bounds.right = (int) (bounds.left + size.getX());
    }

    @Override
    public void update(Game game, long elapsed) {
    }

    private static final Paint debugPaint = new Paint();

    static {
        debugPaint.setStyle(Paint.Style.STROKE);
        debugPaint.setStrokeWidth(2);
    }

    /**
     * Will be called each frame to allow this Entity to render itself
     *
     * @param game - BaseGame which this entity is attached to
     * @param renderer - Renderer through which this entity can perform drawing operations to display itself
     */
    public void render(Game game, Renderer renderer) {
        Rect bounds = getRectangle();
        Canvas canvas = renderer.getCanvas();

        if (bitmap != null) {
            canvas.drawBitmap(bitmap, Renderer.getBitmapRect(bitmap), getRectangle(), null);
        }

        if (renderer.isDebugging(Renderer.DebugFlag.Bounds)) {
            debugPaint.setColor(Color.RED);
            canvas.drawRect(bounds, debugPaint);
        }

        if (renderer.isDebugging(Renderer.DebugFlag.Hitboxes) && this instanceof PhysicsEntity) {
            Rect collisionRectangle = ((PhysicsEntity) this).getCollisionRectangle();

            if (collisionRectangle != null) {
                debugPaint.setColor(Color.GREEN);
                canvas.drawRect(collisionRectangle, debugPaint);
            }
        }
    }

}
