package com.sub.sploosh.engine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.sub.sploosh.game.ScoreKeeper;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Janderson on 08/03/16.
 */
public class ScoreManager {

    public class Score {
        public String name;
        public int score;
    }

    private SQLiteDatabase readableDatabase;
    private SQLiteDatabase writableDatabase;

    private ScoreOpenHelper scoreOpenHelper;

    public ScoreManager(Context context) {
        scoreOpenHelper = new ScoreOpenHelper(context);

        readableDatabase = scoreOpenHelper.getReadableDatabase();
        writableDatabase = scoreOpenHelper.getWritableDatabase();
    }

    public SQLiteQueryBuilder queryBuilder() {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(ScoreOpenHelper.SCORES_TABLE_NAME);

        return queryBuilder;
    }

    public List<Score> getScores() {
        SQLiteQueryBuilder queryBuilder = queryBuilder();
        Cursor cursor = queryBuilder.query(readableDatabase, null, null, null, null, null, "score DESC", "10");

        List<Score> scores = new LinkedList<>();

        while (cursor.moveToNext()) {
            Score score = new Score();

            score.score = cursor.getInt(cursor.getColumnIndex("score"));
            score.name = cursor.getString(cursor.getColumnIndex("name"));

            scores.add(score);
        }

        return scores;
    }

    public byte[] findScoreById(Integer id) {
        SQLiteQueryBuilder queryBuilder = queryBuilder();
        queryBuilder.appendWhere("id = ");
        queryBuilder.appendWhereEscapeString(id.toString());

        Cursor cursor = queryBuilder.query(readableDatabase, null, null, null, null, null, null);

        if (!cursor.moveToFirst()) return null;

        return cursor.getBlob(cursor.getColumnIndex("score"));
    }

    public Integer createScore(String name, int score) {
        ContentValues record = new ContentValues();
        record.put("name", name);
        record.put("score", score);

        long newId = writableDatabase.insert(ScoreOpenHelper.SCORES_TABLE_NAME, null, record);

        return Integer.valueOf((int) newId);
    }

    public boolean deleteScore(Integer id) {
        return writableDatabase.delete(ScoreOpenHelper.SCORES_TABLE_NAME, "id = ?", new String[]{id.toString()}) > 0;
    }
}