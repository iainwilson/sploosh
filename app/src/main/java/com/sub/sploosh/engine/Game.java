package com.sub.sploosh.engine;

import android.content.Context;
import android.view.SurfaceView;

import com.sub.sploosh.engine.math.Vector2;
import com.sub.sploosh.engine.physics.PhysicsWorld;
import com.sub.sploosh.game.Balloon;
import com.sub.sploosh.game.Bird;
import com.sub.sploosh.game.CloudSpawner;
import com.sub.sploosh.game.GameOver;
import com.sub.sploosh.game.Monkey;
import com.sub.sploosh.game.Rocket;
import com.sub.sploosh.game.ScoreKeeper;
import com.sub.sploosh.game.Sky;
import com.sub.sploosh.game.SpawnerController;

public class Game {

    public enum Event {
        Begin,
        Pause,
        Resume,
        GameOver
    }

    public interface EventHandler {
        void handleEvent(Event event);
    }

    protected final GameLoop loop = new GameLoop(this);
    protected final Renderer renderer;
    protected final Context context;
    protected final PhysicsWorld physicsWorld = new PhysicsWorld();
    protected final AssetManager assetManager;
    protected final ScoreKeeper scoreKeeper = new ScoreKeeper();
    protected final FrameRateEntity frameRateEntity = new FrameRateEntity();

    protected Input input;
    protected Monkey monkey;
    protected ScoreManager scoreManager; //this is for the high scores

    public static boolean godMode = false;

    /**
     * This loads game assets into memory before the game starts playing. Improves performance
     */
    private void preLoad() {
        Bird.preLoad(this);
        Rocket.preLoad(this);
        Monkey.preLoad(this);
        Balloon.preLoad(this);
        GameOver.preLoad(this);
    }

    public Game(SurfaceView surfaceView, boolean beginPlayingImmediately) {
        this.context = surfaceView.getContext();
        this.renderer = new SurfaceRenderer(this, surfaceView);
        this.assetManager = new AssetManager(new FileIO(context));

        this.scoreManager = new ScoreManager(context);


        preLoad();

        // Create default input. The user will be able to select the accelerometer from a switch on the main menu
        this.input = new TouchInput(surfaceView);

        this.monkey = new Monkey(this);

        loop.attach(new Sky());
        loop.attach(new CloudSpawner(this));
        //    loop.attach(new Bolt(this));
        loop.attach(monkey);

        if (beginPlayingImmediately) {
            beginPlaying();
        }

        this.physicsWorld.setReference(monkey);
        this.physicsWorld.setReferenceFactor(Vector2.Y_ONLY);
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public void beginPlaying() {
        begun = true;

        loop.attach(scoreKeeper);
        loop.attach(new SpawnerController(this));

        triggerEvent(Event.Begin);
    }

    private boolean begun = false;

    public boolean hasBegun() {
        return begun;
    }

    public boolean isPaused() {
        return loop.isPaused();
    }

    private EventHandler eventHandler;

    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public void setDisplayFPSCounter(boolean display) {
        if (display && !frameRateEntity.isActive()) {
            loop.attach(frameRateEntity);
        }
        else if (!display && frameRateEntity.isActive()) {
            loop.deferredDetach(frameRateEntity);
        }
    }

    public void triggerEvent(Event event) {
        if (eventHandler != null) {
            eventHandler.handleEvent(event);
        }
    }

    public Input getInput() {
        return input;
    }

    public Monkey getMonkeyEntity() {
        return monkey;
    }

    public PhysicsWorld getPhysicsWorld() {
        return physicsWorld;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public GameLoop getLoop() {
        return loop;
    }

    public ScoreManager getScoreManager() {
        return scoreManager;
    }

    public ScoreKeeper getScoreKeeper() {
        return scoreKeeper;
    }

    public void pause() {
        loop.pause();
        assetManager.getSoundPool().autoPause();

        triggerEvent(Event.Pause);
    }

    public void resume() {
        loop.resume();
        assetManager.getSoundPool().autoResume();

        triggerEvent(Event.Resume);
    }

    public void dispose() {
        this.loop.pause();
        this.renderer.dispose();
    }


}
