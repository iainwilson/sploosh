package com.sub.sploosh.engine;

public abstract class LayeredRenderableEntity extends RenderableEntity {
    /**
     * @return Z-order position of this entity. Smaller values are in the background, larger values
     * in the foreground.
     */
    abstract int getLayerIndex();
}
