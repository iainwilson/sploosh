package com.sub.sploosh.engine.physics;

import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.math.Vector2;

/** Represents a world that a group of {@link PhysicsEntity}s belong to */
public class PhysicsWorld {

    /** Acceleration due to gravity */
    public static final Vector2 GRAVITY = new Vector2(0, Renderer.HEIGHT / 20);

    private Vector2 referenceFactor = Vector2.ONE;
    private PhysicsState reference;

    // What all things move relative to
    public PhysicsState getReference() {
        return reference;
    }

    // Set what all things move relative to
    public void setReference(PhysicsState reference) {
        this.reference = reference;
    }

    // Set the entity that all things move relative to
    public void setReference(PhysicsEntity entity) {
        setReference(entity.physicsState);
    }

    /**
     * Can be used to ignore the frame of reference in one axis (e.g Vector2.Y_ONLY)
     */
    public void setReferenceFactor(Vector2 referenceFactor) {
        this.referenceFactor = referenceFactor;
    }

    /**
     * Applies the world's frame of reference to the given velocity
     */
    public void applyReference(Vector2.Mutable velocity) {
        if (reference == null) {
            return;
        }

        velocity.set(
            velocity.getX() - reference.velocity.getX() * referenceFactor.getX(),
            velocity.getY() - reference.velocity.getY() * referenceFactor.getY()
        );
    }

}
