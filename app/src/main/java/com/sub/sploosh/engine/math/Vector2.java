package com.sub.sploosh.engine.math;

import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Two component vector - an (x, y) point
 */
public class Vector2 {

    public static class Mutable extends Vector2 {

        public Mutable() {
            super(0, 0);
        }

        public Mutable(double x, double y) {
            super(x, y);
        }

        public Mutable(Vector2 vector2) {
            this(vector2.x, vector2.y);
        }

        public Vector2.Mutable set(double x, double y) {
            this.x = x;
            this.y = y;

            return this;
        }

        public Vector2.Mutable set(Vector2 other) {
            this.x = other.x;
            this.y = other.y;

            return this;
        }

        public Vector2.Mutable setMagnitudeAndDirection(double magnitude, double direction) {
            double rad = java.lang.Math.toRadians(90 - direction);

            return set(
                +magnitude * java.lang.Math.cos(rad),
                -magnitude * java.lang.Math.sin(rad)
            );
        }

        public Vector2.Mutable multiplyBy(double factor) {
            x *= factor;
            y *= factor;

            return this;
        }

    }

    public static final Vector2 ZERO = new Vector2(0, 0);
    public static final Vector2 HALF = new Vector2(0.5, 0.5);
    public static final Vector2 ONE = new Vector2(1, 1);

    // Multiple by this to get only the X component
    public static final Vector2 X_ONLY = new Vector2(1, 0);

    // Multiple by this to get only the Y component
    public static final Vector2 Y_ONLY = new Vector2(0, 1);

    protected double x, y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Vector2 fromMagnitudeAndDirection(double magnitude, double direction) {
        return new Vector2.Mutable().setMagnitudeAndDirection(magnitude, direction);
    }

    /**
     * @return Vector magnitude (distance if it represents a point, or speed if it represents a velocity)
     */
    public double magnitude() {
        return java.lang.Math.sqrt((x * x) + (y * y));
    }

    public Vector2 perpendicular() {
        return new Vector2(y, -x);
    }

    /**
     * @return Vector with components normalised to be between 0 and 1
     */
    public Vector2 normalized() {
        double magnitude = magnitude();
        return new Vector2(x / magnitude, y / magnitude);
    }

    /**
     * @return Angle, in degrees, that this vector points
     */
    public double direction() {
        return MathUtilities.normalizeAngle(90 - java.lang.Math.toDegrees(java.lang.Math.atan2(-y,
                x)));
    }

    /** Multiply by scalar */
    public Vector2 times(double factor) {
        return new Vector2(x * factor, y * factor);
    }

    /** Multiply by vector */
    public Vector2 times(Vector2 other) {
        return new Vector2(x * other.x, y * other.y);
    }

    public Vector2 dividedBy(Vector2 other) {
        return new Vector2(x / other.x, y / other.y);
    }

    /** Add vector */
    public Vector2 plus(Vector2 other) {
        return new Vector2(x + other.x, y + other.y);
    }

    /** Subtract vector */
    public Vector2 minus(Vector2 other) {
        return new Vector2(x - other.x, y - other.y);
    }

    /** Negate */
    public Vector2 negated() {
        return new Vector2(-x, -y);
    }

    @Override
    public String toString() {
        return String.format("%s (x: %f, y: %f, angle: %f, magnitude: %f)", super.toString(), x, y, direction(), magnitude());
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof Vector2) && (((Vector2) other).x == x && ((Vector2) other).y == y);
    }

    // Rectangle at (0, 0) with this vector as it's size
    public static RectF sizeAtOrigin(Vector2 size) {
        return new RectF(0, 0, (float) size.x, (float) size.y);
    }

    // Calculates a rectangle with the given size, and centre position
    public static RectF calculateSizeCenteredAt(Vector2 size, Vector2 center, RectF result) {
        result.set(
                (float) (center.x - size.x / 2),
                (float) (center.y - size.y / 2),
                (float) (center.x + size.x / 2),
                (float) (center.y + size.y / 2)
        );

        return result;
    }

    // Rectange with given size and center position
    public static RectF sizeCenteredAt(Vector2 size, Vector2 center) {
        return calculateSizeCenteredAt(size, center, new RectF());
    }

    // Rectange with given size and top left position
    public static RectF sizeAt(Vector2 size, Vector2 topLeft) {
        return new RectF(
                (float) topLeft.x,
                (float) topLeft.y,
                (float) (topLeft.x + size.x),
                (float) (topLeft.y + size.y)
        );
    }

    // Convert RectF to Rect by rounding
    public static Rect round(RectF rect) {
        Rect rounded = new Rect();
        rect.round(rounded);

        return rounded;
    }
}
