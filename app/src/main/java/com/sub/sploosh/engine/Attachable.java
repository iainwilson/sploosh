package com.sub.sploosh.engine;

/**
 * Created by Iain on 23/02/16.
 */

/**
 * Entities should implement this interface if they wish to be notified when they are added or removed
 * from the game loop
 */
public interface Attachable {

    /**
     * Called by the game loop when this entity is attached
     */
    void onAttached();

    /**
     * Called by the game loop when this entity is detached
     */
    void onDetached();

}
