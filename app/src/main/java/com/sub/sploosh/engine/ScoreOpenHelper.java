package com.sub.sploosh.engine;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Janderson on 08/03/16.
 */
public class ScoreOpenHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "scores";
    public static final String SCORES_TABLE_NAME = "scores";
    public static final String SCORES_TABLE_CREATE = String.format("CREATE TABLE %s (id INTEGER PRIMARY KEY, name TEXT, score INTEGER)", SCORES_TABLE_NAME);

    ScoreOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCORES_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}