package com.sub.sploosh.engine;

import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import com.sub.sploosh.engine.audio.Music;
import com.sub.sploosh.engine.audio.Sound;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Jason on 05/11/2015.
 */

// Based on the AssetManager in the GAGE engine
public class AssetManager {

    /* Instance Variables */

    private FileIO mFileIO;

    private final HashMap<String, Bitmap> mBitmaps = new HashMap<>();
    private final HashMap<String, Music> mMusic = new HashMap<>();
    private final HashMap<String, Sound> mSounds = new HashMap<>();
    private final SoundPool mSoundPool;


    /* Constructor */

    public AssetManager(FileIO fileIO)
    {
        mFileIO = fileIO;
        mSoundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
    }


    /* Methods */

    //Methods to add assets to HashMap

    public boolean add(String assetName, Bitmap asset)
    {
        if (mBitmaps.containsKey(assetName))
            return false;

        mBitmaps.put(assetName, asset);
        return true;
    }

    public boolean add(String assetName, Music asset)
    {
        if (mBitmaps.containsKey(assetName))
            return false;

        mMusic.put(assetName, asset);
        return true;
    }

    public boolean add(String assetName, Sound asset)
    {
        if (mSounds.containsKey(assetName))
            return false;

        mSounds.put(assetName, asset);
        return true;
    }


    //Methods to load and add assets

    public boolean loadAndAddBitmap(String assetName, String bitmapFile)
    {

        boolean success = true;
        try {
            Bitmap bitmap = mFileIO.loadBitmap(bitmapFile, null);
            success = add(assetName, bitmap);
        } catch (IOException e) {
            Log.e("Error", "AssetManager.loadAndAddBitmap: Cannot load [" + bitmapFile + "]");
            success = false;
        }

        return success;
    }

    public boolean loadAndAddMusic(String assetName, String musicFile)
    {
        boolean success = true;
        try {
            Music music = mFileIO.loadMusic(musicFile);
            success = add(assetName, music);

        } catch (IOException e) {
            Log.e("Gage", "AssetStore.loadAndAddMusic: Cannot load ["
                    + musicFile + "]");
            success = false;
        }

        return success;
    }

    public boolean loadAndAddSound(String assetName, String soundFile)
    {
        boolean success = true;
        try {
            Sound sound = mFileIO.loadSound(soundFile, mSoundPool);
            success = add(assetName, sound);


        } catch (IOException e) {
            Log.e("Gage", "AssetStore.loadAndAddSound: Cannot load ["
                    + soundFile + "]");
            success = false;
        }

        return success;
    }


    //Methods to get assets from the HashMap

    public Bitmap getBitmap(String assetName)
    {
        return mBitmaps.get(assetName);
    }

    public Music getMusic(String assetName)
    {
        return mMusic.get(assetName);
    }

    public Sound getSound(String assetName)
    {
        return mSounds.get(assetName);
    }

    public SoundPool getSoundPool() {
        return mSoundPool;
    }


    //Methods to get assets from HashMap. If the HasMap does not contain the asset it will be added and loaded.

    public Bitmap loadAndAddAndGetBitmap(String assetName, String bitmapFile)
    {
        if (!mBitmaps.containsKey(assetName))
        {
            loadAndAddBitmap(assetName, bitmapFile);
        }
        return mBitmaps.get(assetName);
    }

    public Music loadAndAddAndGetMusic(String assetName, String musicFile)
    {
        if (!mMusic.containsKey(assetName))
        {
            loadAndAddMusic(assetName, musicFile);
        }
        return mMusic.get(assetName);
    }

    public Sound loadAndAddAndGetSound(String assetName, String soundFile)
    {
        if (!mSounds.containsKey(assetName))
            {
                loadAndAddSound(assetName, soundFile);
            }
        return mSounds.get(assetName);
    }


}
