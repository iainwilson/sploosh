package com.sub.sploosh.engine;

import android.os.Handler;
import android.os.SystemClock;

import java.util.Iterator;
import java.util.LinkedList;

import com.sub.sploosh.engine.util.DepthFirstIterator;

public class GameLoop implements Iterable<Entity> {

    private final LinkedList<Entity> entities = new LinkedList<>();

    /**
     * Iterator over all the entities that are part of the game loop
     */
    @Override
    public Iterator<Entity> iterator() {
        return new DepthFirstIterator<Entity>(entities.iterator());
    }

    private final Handler handler = new Handler();

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            synchronized (GameLoop.this) {
                long duration = GameLoop.this.runOnce();
                long delayUntilNextRun = Math.max(0, targetFrameInterval - duration);

                if (!paused) {
                    GameLoop.this.handler.postDelayed(this, delayUntilNextRun);
                }
            }
        }
    };

    private Long lastUpdate = null;

    private final long targetFrameRate = 60;
    private final long targetFrameInterval = 1000 / targetFrameRate;

    private final Game game;

    public GameLoop(Game game) {
        this.game = game;
    }

    private static long getTimeMillis() {
        return SystemClock.elapsedRealtime();
    }

    private boolean paused = false;

    /**
     * Stops the game loop
     */
    public void pause() {
        paused = true;
        handler.removeCallbacks(runnable);
    }

    /**
     * Restarts the game loop after a pause
     */
    public void resume() {
        paused = false;
        lastUpdate = getTimeMillis() - targetFrameInterval;
        handler.postDelayed(runnable, 0);
    }

    public boolean isPaused() {
        return paused;
    }

    /**
     * Updates all entities
     *
     * @param elapsed Time elapsed since last update in milliseconds
     */
    private void update(long elapsed) {
        for (Entity entity : entities) {
            entity.update(this.game, elapsed);
        }

        while (!deferred.isEmpty()) {
            Runnable runnable = deferred.pop();
            runnable.run();
        }
    }

    private LinkedList<Runnable> deferred = new LinkedList<>();

    /**
     * Enqueues a Runnable to be run at the end of the current update cycle
     */
    public void defer(Runnable runnable) {
        deferred.addLast(runnable);
    }

    /**
     * Draws all RenderableEntities in their current state.
     * <p/>
     * TODO: Render objects in order from background to foreground, based on {@link LayeredRenderableEntity#getLayerIndex()}
     */
    public void render() {
        Renderer renderer = game.getRenderer();

        if (!renderer.beginRender()) {
            return;
        }

        for (Entity entity : entities) {
            if (entity instanceof RenderableEntity) {
                ((RenderableEntity) entity).render(game, renderer);
            }
        }

        renderer.endRender();
    }

    /**
     * Produces a single frame by updating and rendering all entities.
     *
     * @return Duration of the update and render in milliseconds
     */
    private long runOnce() {
        long runStart = getTimeMillis();
        long elapsedSinceLastUpdate = targetFrameInterval;

        if (lastUpdate != null) {
            elapsedSinceLastUpdate = runStart - lastUpdate;
        }

        lastUpdate = runStart;

        update(elapsedSinceLastUpdate);
        render();

        long runEnd = getTimeMillis();
        long runDuration = runEnd - runStart;

        return runDuration;
    }

    /**
     * Adds an entity to the game loop. It will be updated and rendered each frame.
     */
    public void attach(Entity entity) {
        entities.add(entity);

        if (entity instanceof Attachable) {
            ((Attachable) entity).onAttached();
        }
    }

    /**
     * Add an entity, in the same way as attach, but after the current update cycle has completed
     */
    public void deferredAttach(final Entity entity) {
        defer(new Runnable() {
            @Override
            public void run() {
                attach(entity);
            }
        });
    }

    public void attachAfter(Entity other, Entity entity) {
        int index = entities.indexOf(other);

        if (index >= 0) {
            entities.add(index, entity);
        }
        else {
            throw new IndexOutOfBoundsException(String.format("Cannot add %s after %s, which is not attached to the game loop", entity, other));
        }
    }

    /**
     * Remove an entity from the game loop. It will no longer be updated or rendered each frame.
     */
    public void detach(final Entity entity) {
        if (entity instanceof Attachable) {
            ((Attachable) entity).onDetached();
        }

        Iterator<Entity> iterator = this.iterator();

        while (iterator.hasNext()) {
            Entity activeEntity = iterator.next();

            if (activeEntity == entity) {
                iterator.remove();
                break;
            }
        }
    }

    /**
     * Remove an entity, in the same way as detach, but after the current update cycle has completed
     */
    public void deferredDetach(final Entity entity) {
        defer(new Runnable() {
            @Override
            public void run() {
                detach(entity);
            }
        });
    }

    public void replace(Entity oldEntity, Entity newEntity) {
        attachAfter(oldEntity, newEntity);

        if (oldEntity != null) {
            detach(oldEntity);
        }
    }

}
