package com.sub.sploosh.engine.math;

import java.util.Random;

public class MathUtilities {

    private static final Random random = new Random(/* seed */);

    /**
     * @param start Lower end of range
     * @param end   Upper end of range
     * @return A random integer >= start and <= end
     */
    public static int randomInRange(int start, int end) {
        return start + random.nextInt(end - start);
    }

    /**
     * @param angle Any angle (in degrees)
     * @return An angle equivalent to the given angle, in the range -180 to +180
     */
    public static double normalizeAngle(double angle) {
        angle %= 360;

        if (angle < -180) {
            angle += 360;
        }
        else if (angle > 180) {
            angle -= 360;
        }

        return angle;
    }

}
