package com.sub.sploosh.engine;

import java.util.Collections;
import java.util.Iterator;

/**
 * Represents an in-game entity which should be updated on every iteration of the game loop.
 */

public abstract class Entity implements Attachable, Iterable<Entity> {

    /**
     * @param game BaseGame to which this entity is attached
     * @param elapsed Duration in milliseconds since this entity was last updated
     */
    public abstract void update(Game game, long elapsed);

    private boolean active = false;

    @Override
    public void onAttached() {
        active = true;
    }

    @Override
    public void onDetached() {
        active = false;
    }

    public boolean isActive() {
        return active;
    }

    /**
     * Iterator over any sub-entities managed by this entity
     */
    @Override
    public Iterator<Entity> iterator() {
        return Collections.emptyIterator();
    }

}
