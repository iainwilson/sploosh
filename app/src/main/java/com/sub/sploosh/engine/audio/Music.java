package com.sub.sploosh.engine.audio;

/**
 * Created by James Anderson
 */

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import java.io.IOException;

public class Music {
    protected MediaPlayer mediaPlayer = new MediaPlayer();

    public Music(AssetFileDescriptor assetFileDescriptor) {
        try {
            mediaPlayer.setDataSource(
                    assetFileDescriptor.getFileDescriptor(),
                    assetFileDescriptor.getStartOffset(),
                    assetFileDescriptor.getLength()
            );

            mediaPlayer.setLooping(true);
            mediaPlayer.prepare();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void play(){
        mediaPlayer.start();
    }
    public void pause() {
        mediaPlayer.pause();
    }
    public void stop() {
        mediaPlayer.stop();
    }
    public void playFromStart(){mediaPlayer.seekTo(0); mediaPlayer.start();}

}
