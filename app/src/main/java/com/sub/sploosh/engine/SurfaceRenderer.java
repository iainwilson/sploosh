package com.sub.sploosh.engine;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.sub.sploosh.engine.math.Vector2;

/**
 * Implementation of {@link Renderer} which draws to a {@link SurfaceView}
 */
public class SurfaceRenderer extends Renderer implements SurfaceHolder.Callback {

    private final Game game;
    private final SurfaceView surfaceView;
    private final Rect gameBounds = new Rect(0, 0, (int) WIDTH, (int) HEIGHT);
    private final Rect canvasBounds;
    private final Vector2.Mutable size = new Vector2.Mutable(gameBounds.width(), gameBounds.height());
    private Canvas canvas;
    private Paint backgroundPaint = new Paint(0);
    private Matrix matrix = new Matrix();

    /**
     *
     * @param game BaseGame which will be rendered
     * @param surfaceView Surface which will be drawn onto
     */
    public SurfaceRenderer(Game game, SurfaceView surfaceView) {
        this.game = game;
        this.surfaceView = surfaceView;

        this.canvasBounds = new Rect(0, 0, surfaceView.getWidth(), surfaceView.getHeight());

        // Be notified when the surface changes
        this.surfaceView.getHolder().addCallback(this);
    }

    @Override
    public Canvas getCanvas() {
        return canvas;
    }

    public boolean beginRender() {
        if (canvas != null) {
            // Already rendering
            return false;
        }

        if (surfaceView.getHolder().isCreating()) {
            // Not ready yet
            return false;
        }

        canvas = surfaceView.getHolder().lockCanvas();

        if (canvas == null) {
            // Surface unavailable
            return false;
        }

        configure();

        // Clear canvas
        canvas.drawRect(new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), backgroundPaint);

        canvas.save();
        canvas.setMatrix(matrix);

        return true;
    }

    public void endRender() {
        canvas.restore();

        surfaceView.getHolder().unlockCanvasAndPost(canvas);
        canvas = null;
    }

    /**
     * Forces the current frame to be rerendered - e.g. when the screen size changes due to rotation.
     */
    private void refresh() {
        if (beginRender()) {
            game.getLoop().render();
            endRender();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        configure();
        refresh();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        configure();
        refresh();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    private void configure() {
        canvasBounds.set(0, 0, surfaceView.getWidth(), surfaceView.getHeight());

        float scale = (float) Math.min(canvasBounds.width() / WIDTH, canvasBounds.height() / HEIGHT);

        float w = (float) WIDTH * scale;
        float h = (float) HEIGHT * scale;
        float tx = (canvasBounds.width() - w) / 2.f;
        float ty = (canvasBounds.height() - h) / 2.f;

        gameBounds.set(
                -(int)(tx / scale),
                -(int)(ty / scale),
                (int) ((2 * tx / scale + w) / scale),
                (int) ((2 * ty / scale + h) / scale)
        );

        size.set(
            gameBounds.width(),
            gameBounds.height()
        );

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scale, scale, 0, 0);

        Matrix translateMatrix = new Matrix();
        translateMatrix.setTranslate(tx, ty);

        matrix.setConcat(translateMatrix, scaleMatrix);
    }

    @Override
    public Vector2 getSize() {
        return size;
    }

    @Override
    public Rect getBounds() {
        return gameBounds;
    }

    @Override
    public void setBackgroundPaint(Paint backgroundPaint) {
        this.backgroundPaint = backgroundPaint;
    }

    @Override
    public void dispose() {
        this.surfaceView.getHolder().removeCallback(this);
    }
}
