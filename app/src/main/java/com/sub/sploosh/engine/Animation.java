package com.sub.sploosh.engine;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.sub.sploosh.engine.math.Vector2;

/**
 * Sprite sheet animation
 */
public class Animation {

    private final Bitmap sprite;
    private final Vector2 frameSize;

    private final long frameDuration;
    private final int frameCount;

    /**
     *
     * @param sprite Bitmap containing animation frames, laid out horizontally from left to right
     * @param frameSize The pixel dimensions of each animation frame within the sprite
     * @param frameDuration Duration of each frame in milliseconds
     */
    public Animation(Bitmap sprite, Vector2 frameSize, long frameDuration) {
        this.sprite = sprite;
        this.frameSize = frameSize;
        this.frameDuration = frameDuration;

        if (SurfaceRenderer.getBitmapSize(sprite).getX() % frameSize.getX() != 0) {
            throw new Error("Invalid frame size");
        }

        this.frameCount = (int) (SurfaceRenderer.getBitmapSize(sprite).getX() / frameSize.getX());
    }

    public Bitmap getSprite() {
        return sprite;
    }

    public Vector2 getFrameSize() {
        return frameSize;
    }

    /** @return Number of frames found in the sprite bitmap */
    public int getNumberOfFrames() {
        return frameCount;
    }

    /** @return Duration of one loop of the animation */
    public long getTotalDuration() {
        return frameDuration * frameCount;
    }

    /** @return Rectangle within the sprite bitmap for a given frame */
    public Rect calculateRectForFrame(int frameNumber, Rect result) {
        int x = (int) frameSize.getX() * frameNumber;

        result.set(
            x,
            0,
            x + (int) frameSize.getX(),
            (int) frameSize.getY()
        );

        return result;
    }

    /** @return An animation player for playback of an instance of this animation */
    public Tracker createTracker() {
        return new Tracker();
    }

    /**
     * Class which controls which frame of the sprite sheet is currently visible
     */
    public class Tracker {

        private long time = 0;
        private final Rect sourceRect = new Rect();

        public Tracker() {
            calculateRectForFrame(0, sourceRect);
        }

        /**
         *
         * @param elapsed
         */
        public void advance(long elapsed) {
            time += elapsed;
            time %= getTotalDuration();

            calculateRectForFrame(getFrameNumber(), sourceRect);
        }

        public int getFrameNumber() {
            return (int) (time / frameDuration);
        }

        /**
         * @return Source rectangle within the sprite bitmap for the current frame
         */
        public Rect getRect() {
            return sourceRect;
        }

        /** Draw the current frame on the canvas in the bounds rectangle */
        public void draw(Canvas canvas, Rect bounds) {
            canvas.drawBitmap(sprite, getRect(), bounds, null);
        }

    }

}
