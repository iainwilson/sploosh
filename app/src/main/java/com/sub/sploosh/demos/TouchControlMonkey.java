package com.sub.sploosh.demos;

import android.app.Activity;
import android.app.Fragment;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sub.sploosh.R;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Liam Mullen  on 25/11/2015.
 */
public class TouchControlMonkey extends Fragment {

    /*
	 * (non-Javadoc)
	 *
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */

    /*
      * (non-Javadoc)
      * @see android.app.Fragment#onCreateView(android.view.LayoutInflater,
      * android.view.ViewGroup, android.os.Bundle)
      */
    RenderView renderview;
    AssetManager assetManager;
    View view;
    ImageView imageView;
    TextView directionTextView;

    private Matrix matrix = new Matrix();
    Paint mPaint;
    /**
     * Variables that will control bitmap animation
     */
    private float scale = 1.0f;
    private float rotation =0.0f;
    private float offset = 0.0f;
    private float scaleDir = 1.0f, offsetDir = 1.0f;

    private Rect source = new Rect();
    private Rect destination = new Rect();
    Bitmap bitmap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        // Inflate the view
        renderview = new RenderView(getActivity());

        view = inflater.inflate(R.layout.touch_control_monkey,
                container, false);
        assetManager = getActivity().getAssets();
        renderview = new RenderView(getActivity());
        final Bitmap bitmap = loadBitmap(assetManager, "imgLiam/monkeysprite1.png");

//        // ATTENTION LAVANDER LOCKS: The ImageView in the XML would be a good spot for the monkey
        final ImageView imageView = (ImageView) view
                .findViewById(R.id.touch_input_imageview);

        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);

            //image transforms

            imageView.refreshDrawableState();
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
            matrix.reset();
            matrix = imageView.getImageMatrix();
            matrix.setScale(0.5f, 0.5f, imageView.getWidth() / 2, imageView.getHeight() / 2);
            matrix.postRotate(rotation, 50.0f, 50.0f);
            matrix.postTranslate(50.0f + offset, 50.0f); // Finally translate
            imageView.setImageMatrix(matrix);

        }


        final TextView directionTextView = (TextView) view
                .findViewById(R.id.touch_input_direction);

        TextView lefttouchArea = (TextView) view
                .findViewById(R.id.touch_input_event_lefttoucharea);
        lefttouchArea.setOnTouchListener(new SingleTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    rotation = 0.0f;
                    matrix.postRotate(rotation) ;
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    directionTextView.setText("Left");

                    //ATTENTION LAVANDER LOCKS: Heres where you can put the code which moves the monkey left
                    rotation = -15.0f;
                    new RenderView(getActivity());
                    matrix = imageView.getImageMatrix();
                    matrix.postRotate(rotation, 50.0f, 50.0f);
                }
                if (event.getAction() == MotionEvent.ACTION_MOVE) {

                }


                try {
                    // Go to sleep for 20ms (i.e. target 50FPS)
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                }
                // Invalid our canvas, so we'll be asked to re-draw
                // invalidate;

                return false;
            }
        });

        // Get the 'lefttouch' area and add a touch listener to it
        TextView righttouchArea = (TextView) view
                .findViewById(R.id.touch_input_event_righttoucharea);
        righttouchArea.setOnTouchListener(new SingleTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    rotation = 0.0f;
                    matrix = imageView.getImageMatrix();
                    matrix.postRotate(rotation, 50.0f, 50.0f);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    directionTextView.setText("Right");
                    //ATTENTION LAVANDER LOCKS: Heres where you can put the code which moves the monkey right
                    rotation = 15.0f;
                    matrix = imageView.getImageMatrix();

                    scale += scaleDir * 0.02f;
                    if (scale < 0.75f)
                        scaleDir = 1.0f;
                    else  if (scale > 1.25f)
                        scaleDir = -1.0f;
                    offset += offsetDir * 5.0f;
                    if (offset < -180) {
                        offsetDir = 1.0f;

                    }

                    else if (offset > 700)
                    {
                        offsetDir = -1.0f;
                        rotation = -15.0f;
                    }
                    imageView.refreshDrawableState();
                    imageView.setScaleType(ImageView.ScaleType.MATRIX);
                    matrix.reset();
                    matrix = imageView.getImageMatrix();
                    matrix.setScale(0.5f, 0.5f, imageView.getWidth() / 2, imageView.getHeight() / 2);
                    matrix.postRotate(rotation);
                    matrix.postTranslate(50.0f + offset, 50.0f); // Finally translate


                }
                if (event.getAction() == MotionEvent.ACTION_MOVE) {

                }


                return false;
            }
        });




//
//        try
//        {
//            // Go to sleep for 20ms (i.e. target 50FPS)
//            Thread.sleep(20);
//        }
//        catch (InterruptedException e) {}
//        // Invalid our canvas, so we'll be asked to re-draw
//        //    invalidate();

//===============================
//ATTENTION CLINT EASTWOOD, IF YOU CHANGE THESE 2 LINES OF CODE BELOW
//        MY METHOD FOR RENDERING SHOULD RUN
//return new RenderView(getActivity());
//      RenderView renderView = new RenderView;
//        setContentView(renderView);

        return view;

    }



    // ////////////////////////////////////////////////////////////////////////
    // Single Touch Event Handler
    // ////////////////////////////////////////////////////////////////////////

    /**
     * Listener class that can support a single touch event (not that useful but
     * it acts as a basis on which a multi-touch listener can be constructed).
     */
    @SuppressLint("ClickableViewAccessibility")
    private class SingleTouchListener implements OnTouchListener {

        // ////////////////////////////////////////////////////////////////////
        // Touch Events
        // ////////////////////////////////////////////////////////////////////

        /**
         * Remember the last motion event that occurred (used to determine if a
         * move event has concluded and to limit the number of events that are
         * output).
         */
        private int mLastMotionEvent = MotionEvent.ACTION_CANCEL;

        /*
         * (non-Javadoc)
         *
         * @see android.view.View.OnTouchListener#onTouch(android.view.View,
         * android.view.MotionEvent)
         */
        @Override
        public boolean onTouch(View v, MotionEvent event) {

//            // Extract the single-touch event type and location
//            TextView lefttouchArea = (TextView) view
//                    .findViewById(R.id.touch_input_event_lefttoucharea);
//            lefttouchArea.setOnTouchListener(new SingleTouchListener(){
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    if(event.getAction()==MotionEvent.ACTION_UP)
//                    {
//
//                    }
//                    if(event.getAction()==MotionEvent.ACTION_DOWN)
//                    {
//                        directionTextView.setText("Left");
//
//                    }
//                    if(event.getAction()==MotionEvent.ACTION_MOVE)
//                    {
//                        //code for image moving
//                    }
//                    return false;
//                }
//            });
//
//            // Get the 'lefttouch' area and add a touch listener to it
//            TextView righttouchArea = (TextView) view
//                    .findViewById(R.id.touch_input_event_righttoucharea);
//            righttouchArea.setOnTouchListener(new SingleTouchListener(){
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    if(event.getAction()==MotionEvent.ACTION_UP)
//                    {
//
//                    }
//                    if(event.getAction()==MotionEvent.ACTION_DOWN)
//                    {
//                        directionTextView.setText("Right");
//
//                    }
//                    if(event.getAction()==MotionEvent.ACTION_MOVE)
//                    {
//                        //code for image moving
//                    }
//                    return false;
//                }
//            });
//
            return true;
        }

        // ////////////////////////////////////////////////////////////////////
        // Touch Info Display
        // ////////////////////////////////////////////////////////////////////

        /**
         * String builder to help put together the output strings
         */
        private StringBuilder mEventDetails = new StringBuilder();

        /**
         * Display the x and y touch location
         *
         * @param x
         *            Location of the touch event on the x axis
         * @param y
         *            Location of the touch event on the y axis
         */
        private void displayTouchPosition(float x, float y) {
            TextView xLoc = (TextView) getView().findViewById(
                    R.id.touch_input_X_loc);
            xLoc.setText(String.format("%.1f", x));
            TextView yLoc = (TextView) getView().findViewById(
                    R.id.touch_input_Y_loc);
            yLoc.setText(String.format("%.1f", y));
        }

        /**
         * Display details of the event that has occurred
         *
         * @param event
         *            Event descriptor
         * @param x
         *            Location of event along x-axis
         * @param y
         *            Location of event along y-axis
         */
        private void displayEvent(String event, float x, float y) {

            mEventDetails.append(String.format("%-15s", event));
            mEventDetails.append(" [");
            mEventDetails.append(String.format("%.1f", x));
            mEventDetails.append(",");
            mEventDetails.append(String.format("%.1f", y));
            mEventDetails.append("]\n");

            TextView eventTextView = (TextView) getView().findViewById(
                    R.id.touch_input_event_textview);
            eventTextView.setText(mEventDetails.toString());
        }
    }

    private Bitmap loadBitmap(AssetManager assetManager, String asset) {

        Bitmap bitmap = null;
        InputStream inputStream = null;

        try {
            // Try to open the bitmap
            inputStream = assetManager.open(asset);

            // Setup what load preferences we might have (this could be an
            // argument)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            // Load the bitmap
            bitmap = BitmapFactory.decodeStream(inputStream, null, options);

        } catch (IOException e) {
            Log.e("gp9",
                    "Error loading bitmap: " + e.getMessage());
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) { /* Let's just return with a null */ }
        }

        return bitmap;
    }


    private class RenderView extends View {




        /**
         * Bitmap image that will be drawn
         */
        private Bitmap bitmap;

        /**
         * Paint instance for the bitmap
         */
        private Paint mPaint;

        /**
         * Source and destination rectangles
         */
        private Rect source = new Rect();
        private Rect destination = new Rect();

        /**
         * Matrix instance that will be used to demonstrate render options
         */
        private Matrix matrix = new Matrix();

        /**
         * Variables that will control bitmap animation
         */
        private float scale = 1.0f;
        private float rotation = 0.0f;
        private float offset = 0.0f;
        private float scaleDir = 1.0f, offsetDir = 1.0f;


        public RenderView(Context context) {
            super(context);
//        // ATTENTION CLINTY : i just copied your xml down here to try something out

            bitmap = loadBitmap(assetManager, "imgLiam/monkeysprite1.png");

//        // ATTENTION LAVANDER LOCKS: The ImageView in the XML would be a good spot for the monkey
            final ImageView imageView = (ImageView) view
                    .findViewById(R.id.touch_input_imageview);

            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            }
        }

//        super(context);try {
//            AssetManager assetManager = getActivity().getAssets();
//            InputStream inputStream = assetManager
//                    .open("imgLiam/monkeysprites.png");
//
//
//            mImage = BitmapFactory.decodeStream(inputStream);
//            inputStream.close();
//        } catch (IOException e) {
//            Log.d("Demos",
//                    "Load error: " + e.getMessage());
//        }





        @Override
        protected void onDraw(Canvas canvas) {


            scale += scaleDir * 0.02f;
            if (scale < 0.75f)
                scaleDir = 1.0f;
            else if (scale > 1.25f)
                scaleDir = -1.0f;

            offset += offsetDir * 5.0f;
            //    rotation += rotation + 1.0f;
            if (offset < -180) {
                offsetDir = 1.0f;
                //the player will tilt 15 degrees right or left depending on
                //the direction they tilt the device
                rotation = 15.0f;
            } else if (offset > 700) {
                offsetDir = -1.0f;
                rotation = -15.0f;
            }

            matrix.reset();
       //     source.set(300, 10, 540, 250);//wide deep ....420
         //   destination.set(410, 55, 560, 310); //left, top, right, bottom.

            matrix.postScale(0.4f, 0.4f);
            matrix.postRotate(rotation, 50.0f, 50.0f);
            matrix.postTranslate(150.0f + offset, 700.0f); // Finally translate

            //bitmap
            canvas.drawBitmap(bitmap, matrix, mPaint);

            //ATTENTION CLINTY
            //if everything correctly draws to the canvas, can we get the canvas inside an imageView
          //  imageView.draw(canvas);


            try {
                // Go to sleep for 20ms (i.e. target 50FPS)
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
            // Invalid our canvas, so we'll be asked to re-draw
            invalidate();
        }

    }



}

/*

    //ATTENTION LAVANDER LOCKS: Good luck
            _..,----,.._
         .-;'-.,____,.-';
        (( |            |
        `))            ;
          ` \          /
         .-' `,.____.,' '-.
        (     '------'     )
        `-=..________..--'




           _....----"""----...._
        .-'  o    o    o    o   '-.
       /  o    o    o         o    \
    __/__o___o_ _ o___ _ o_ o_ _ _o_\__
   /                                   \
   \___________________________________/
     \~`-`.__.`-~`._.~`-`~.-~.__.~`-`/
      \                             /
       `-._______________________.-'


 //MMMMMMMMMMMMMMMM BUILD A BURGER
 */


