package com.sub.sploosh.demos;

import android.app.Fragment;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class TransformsActivity extends SingleFragmentActivity {

    /*
	 * Return the fragment to be set for this activity within the superclass'
	 * onCreate method
	 *
	 * @see uk.ac.qub.eeecs.demos.SingleFragmentActivity#createFragment()
	 */
    @Override
    public Fragment createFragment() {
        return new TransformsFragment();
    }

}
