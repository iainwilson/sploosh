package com.sub.sploosh.demos;

import android.app.Fragment;

/**
 * Created by Liam Mullen
 */

public class TouchInput extends SingleFragmentActivity {


    /*
     * Return the fragment to be set for this activity within the superclass'
     * onCreate method
     *
     * @see uk.ac.qub.eeecs.demos.SingleFragmentActivity#createFragment()
     */
    @Override
    public Fragment createFragment() {
        return new TouchInputFragment();
    }


}
