package com.sub.sploosh.demos;

/**
 * Created by Liam Mullen on 25/11/2015.
 */

import android.app.Fragment;
public class MonkeyControl extends SingleFragmentActivity {

    @Override
    public Fragment createFragment() {
        return new TouchControlMonkey();
    }
}
