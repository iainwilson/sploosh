package com.sub.sploosh.demos;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.sub.sploosh.Application;
import com.sub.sploosh.MenuActivity;
import com.sub.sploosh.R;
import com.sub.sploosh.ScoresActivity;
import com.sub.sploosh.engine.Accelerometer;
import com.sub.sploosh.engine.Game;
import com.sub.sploosh.engine.Renderer;
import com.sub.sploosh.engine.TouchInput;
import com.sub.sploosh.engine.audio.Music;
import com.sub.sploosh.game.GameMenuFragment;
import com.sub.sploosh.game.GameOverMenuFragment;
import com.sub.sploosh.game.Preference;

public class GameActivity extends Activity implements Game.EventHandler, GameMenuFragment.GameMenuActionListener, GameOverMenuFragment.GameOverMenuActionListener {

    private Game game;
    private RelativeLayout layout;
    private ImageButton playPauseButton;
    private Button disableGodModeButton;

    /**
     * Menu fragments
     */
    private Fragment mainMenuFragment, gameOverMenuFragment;

    /**
     * music that during gameplay and on main menu
     */
    private Music backgroundMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Window window = getWindow();

        /**
         * Ensures that the screen doesn't dim when the user is playing. The user is asked
         * on install if they agree with this permission
         */
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        /**
         * This removes the default title on the screen so that the game plays in full screen
         */

        window.requestFeature(Window.FEATURE_NO_TITLE);
        /**
         * This removes the default title on the screen so that the game plays in full screen
         */
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);
        this.layout = (RelativeLayout) findViewById(R.id.layout);

        this.mainMenuFragment = new GameMenuFragment();
        this.gameOverMenuFragment = new GameOverMenuFragment();

        this.playPauseButton = new ImageButton(this);
        this.playPauseButton.setBackgroundColor(Color.GRAY);

        this.playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (game.isPaused()) {
                    game.resume();
                } else {
                    game.pause();
                }
            }
        });

        float pixel = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());
        int buttonSize = (int) (50 * pixel);
        int paddingSize = (int) (10 * pixel);

        this.disableGodModeButton = new Button(this);
        this.disableGodModeButton.setText("Disable God Mode");
        this.disableGodModeButton.setVisibility(View.INVISIBLE);
        this.disableGodModeButton.setPadding(paddingSize, 0, paddingSize, 0);
        this.disableGodModeButton.setHeight(buttonSize);
        this.disableGodModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                Preference.godMode.disable();
                button.setVisibility(View.INVISIBLE);
            }
        });

        this.layout.addView(this.playPauseButton, new RelativeLayout.LayoutParams(buttonSize, buttonSize));
        this.layout.addView(this.disableGodModeButton);

        this.layout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int
                    oldLeft, int oldTop, int oldRight, int oldBottom) {
                float width = playPauseButton.getWidth();
                float height = playPauseButton.getHeight();

                float padding = 0.5f * width;

                playPauseButton.setX(right - padding - width);
                playPauseButton.setY(bottom - padding - height);

                disableGodModeButton.setX(padding);
                disableGodModeButton.setY(bottom - padding - height);
            }
        });

        //Main menu is displayed
        addFragment(mainMenuFragment);
        createGame(false);
        updatePlayPauseButton();

        /**
         * background music is loaded, for now uncomment the one you want to play and comment the other
         */
        this.backgroundMusic = game.getAssetManager().loadAndAddAndGetMusic("Spyro", "audio/Spyro.mp3");
//        this.backgroundMusic = game.getAssetManager().loadAndAddAndGetMusic("FreeFallin", "audio/FreeFallin.mp3");
    }

    private void displayMainMenu() {
        this.mainMenuFragment = new GameMenuFragment();
        addFragment(this.mainMenuFragment);
    }

    /**
     * Displays a fragment on the screen
     */
    private void addFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .add(R.id.layout, fragment, null)
                .disallowAddToBackStack()
                .commit();

        getFragmentManager().executePendingTransactions();
    }

    /**
     * Removes a fragment from the screen
     */
    private void removeFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .remove(fragment)
                .commit();

        getFragmentManager().executePendingTransactions();
    }

    /**
     * Anything that happens when the game is in here
      */
    @Override
    protected void onResume() {
        super.onResume();

        getSurfaceView().post(new Runnable() {
            @Override
            public void run() {
                resumeGame();
            }
        });
        this.backgroundMusic.play();
    }

    /**
     * Things that need paused when the game is in a 'pause' state is in here.
     * (Paused state- The user may close the app with it running in the background or when the user opens the multi tasking view)
     */
    @Override
    protected void onPause() {
        super.onPause();

        getSurfaceView().post(new Runnable() {
            @Override
            public void run() {
                pauseGame();
            }
        });

        backgroundMusic.pause();
    }

    /**
     * When the user closes the app in the multitasking view
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (game != null) {
            game.dispose();
            game = null;
        }
    }

    private SurfaceView getSurfaceView() {
        //Allows us to link into the xml file and find it by ID which is surface view
        return (SurfaceView) findViewById(R.id.surfaceView);
    }

    // Resets the game state
    private void reset(final boolean beginPlaying) {
        removeFragment(mainMenuFragment);
        removeFragment(gameOverMenuFragment);

        if (game != null) {
            game.dispose();
            game = null;
        }

        // Wait until the surface is created and laid out before starting the game
        createGame(beginPlaying);
        Preference.updateAll();

        resumeGame();
    }

    private void createGame(boolean beginPlaingImmediately) {
        game = new Game(getSurfaceView(), beginPlaingImmediately);
        game.setEventHandler(this);
    }

    /**
     * Pauses the game
     */
    private void pauseGame() {
        game.pause();
    }

    /**
     * resumes the game from where user left off after pause is called
     */
    private void resumeGame() {
        game.resume();
    }

    public void handleEvent(Game.Event event) {
        if (event == Game.Event.Begin) {
            this.disableGodModeButton.setVisibility(Game.godMode ? View.VISIBLE : View.INVISIBLE);
        }
        else if (event == Game.Event.GameOver) {
            this.disableGodModeButton.setVisibility(View.INVISIBLE);
            addFragment(gameOverMenuFragment);
        }

        updatePlayPauseButton();
    }

    private void updatePlayPauseButton() {
        int icon;

        if (game.isPaused()) {
            icon = android.R.drawable.ic_media_play;
        }
        else {
            icon = android.R.drawable.ic_media_pause;
        }

        this.playPauseButton.setImageResource(icon);

        if (game.hasBegun() && !mainMenuFragment.isAdded() && !gameOverMenuFragment.isAdded()) {
            this.playPauseButton.setVisibility(View.VISIBLE);
            this.playPauseButton.bringToFront();
        }
        else {
            this.playPauseButton.setVisibility(View.INVISIBLE);
        }
    }

    // -----------------------------------------
    // Handles events from the game menu fragment
    // -----------------------------------------

    // Handles the play button being clicked on the game menu fragment
    public void onPlayClicked() {
        // Main menu is removed;
        removeFragment(mainMenuFragment);

        // Game begins playing
        game.beginPlaying();
    }

    public void onHighScoresClicked() {
        Intent intent = new Intent(this, ScoresActivity.class);
        startActivity(intent);
    }

    public void onDemosClicked() {
        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }

    /**
     * This sets the input type for the game based on the users preferences on the Switch on the main menu
     * @param useAccelerometer
     */
    public void onToggleControls(boolean useAccelerometer) {
        if (useAccelerometer) {
            System.out.println("Using accelerometer");
            game.setInput(new Accelerometer(this));
        }
        else {
            System.out.println("Using touch input");
            game.setInput(new TouchInput(getSurfaceView()));
        }
    }

    public void onToggleFrameRate(boolean state) {
        game.setDisplayFPSCounter(state);
    }

    public void onToggleDebugFlag(Renderer.DebugFlag flag, boolean state) {
        game.getRenderer().setDebugging(flag, state);
    }

    @Override
    public void onToggleGodMode(boolean state) {
        Game.godMode = state;
    }

    // -----------------------------------------
    // Handles events from the game over menu fragment
    // -----------------------------------------

    @Override
    public void onSaveScore(String name) {
        game.getScoreManager().createScore(name, game.getScoreKeeper().getScore());
    }

    @Override
    public void onPlayAgainClicked() {
        reset(true);
    }

    @Override
    public void onBackClicked() {
        reset(false);

        removeFragment(gameOverMenuFragment);
        displayMainMenu();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Manually handle the back button
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && game.hasBegun()) {
            onBackClicked();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
