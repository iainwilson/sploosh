package com.sub.sploosh.demos;

import android.app.Fragment;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.io.InputStream;

/*
 * Created by Fiona on 02/11/2015.
 */
public class TransformsFragment extends Fragment{

    /*
	 * (non-Javadoc)
	 *
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Create a custom view object that will provide various bitmap drawing
        // examples
        return new RenderView(getActivity());
    }

    /**
     * Custom view object that will display a range of bitmap drawing options.
     * The view will automatically invalidate itself so that it is continuously
     * redrawn as a means of providing an animated display.
     */
    private class RenderView extends View {

        /**
         * Bitmap image that will be drawn
         */
        private Bitmap mImage;

        /**
         * Paint instance for the bitmap
         */
        private Paint mPaint;

        /**
         * Source and destination rectangles
         */
        private Rect source = new Rect();
        private Rect destination = new Rect();

        /**
         * Matrix instance that will be used to demonstrate render options
         */
        private Matrix matrix = new Matrix();

        /**
         * Variables that will control bitmap animation
         */
        private float scale = 1.0f;
        private float rotation = 0.0f;
        private float offset = 0.0f;
        private float scaleDir = 1.0f, offsetDir = 1.0f;



        //////////////////////////////////////////////////////////////////////////////////
        private Bitmap monkeyArr [];

        /**
         * Create a new render view instance
         *
         * @param context
         *            Parent context
         */
        public RenderView(Context context) {
            super(context);

            // Create a new paint object
            mPaint = new Paint();

            // Attempt to load the bitmap
            try {
                AssetManager assetManager = getActivity().getAssets();
                InputStream inputStream = assetManager
                        .open("imgLiam/monkeysprites.png");


                mImage = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            } catch (IOException e) {
                Log.d("Demos",
                        "Load error: " + e.getMessage());
            }
        }

        /*
         * (non-Javadoc)
         *
         * @see android.view.View#onDraw(android.graphics.Canvas)
         */
        @Override
        protected void onDraw(Canvas canvas) {

            //This piece of code manipulates an area of the sprite sheet, which we
            //will use as our main character
            matrix.reset();
            source.set(300, 10, 540, 250);//wide deep ....420
            destination.set(400, 50, 550, 300);

            //400 = location x axis,
            //300 = size/scale
            //550 = width
            //50 =
            //450 worked as width
            //drawing the image with the co-ordinates stated above

            Rect rect = new Rect();
       //     rect = drawBitmap();
            canvas.drawBitmap(mImage, source, destination, mPaint);

            // ////////////////////////////////////////////////////////////////
            // Drawing using a matrix
            // ////////////////////////////////////////////////////////////////

            /**
             * Matrix scaling (you could also use the source/destination
             * rectangle to achieve the same effect)
             */

            // Draw with a 30% x and y scale at the specified location
            matrix.reset();
            matrix.postScale(0.3f, 0.3f);

            //Rotation of the monkey when the player moves right
            matrix.postRotate(15.0f, 50.0f, 50.0f);
            matrix.postTranslate(50.0f, 50.0f); //position along x, y
            canvas.drawBitmap(mImage, matrix, mPaint);

          //  canvas.drawBitmap(mImage, source, destination,matrix, mPaint);


            //If the player moves to the left, the monkey will tilt the opposite way
            matrix.reset();
            matrix.postScale(0.3f, 0.3f);
            matrix.postRotate(-15.0f, 50.0f, 50.0f);
            matrix.postTranslate(150.0f, 250.0f); //position along x, y
            canvas.drawBitmap(mImage, matrix, mPaint);


            //The player can move either right or left depending on the input
            // Update the scale and offset values
            scale += scaleDir * 0.02f;
            if (scale < 0.75f)
                scaleDir = 1.0f;
          else  if (scale > 1.25f)
                scaleDir = -1.0f;

            offset += offsetDir * 5.0f;
        //    rotation += rotation + 1.0f;
            if (offset < -180)
            {
                offsetDir = 1.0f;
                 //the player will tilt 15 degrees right or left depending on
                //the direction they tilt the device
                rotation = 15.0f;
            }
            else if  (offset > 700)
            {
               offsetDir = -1.0f;
                rotation = -15.0f;
            }

            matrix.reset();
            source.set(300, 10, 540, 250);//wide deep ....420
            destination.set(410, 55, 560, 310); //left, top, right, bottom.

           matrix.postScale(0.4f, 0.4f);
           matrix.postRotate(rotation, 50.0f, 50.0f);
            matrix.postTranslate(150.0f + offset, 700.0f); // Finally translate

canvas.drawBitmap(mImage, matrix, mPaint);
            try
            {
                // Go to sleep for 20ms (i.e. target 50FPS)
                Thread.sleep(20);
            }
            catch (InterruptedException e) {}
            // Invalid our canvas, so we'll be asked to re-draw
            invalidate();

            //Combining all these together - (ISROT)
            //Identity
//            matrix.reset();
//            //Scale along x/y axis
//            matrix.setScale(0.3f, 0.3f);
//            //Rotate around objects centre
//            matrix.postRotate(-15.0f, 50.0f, 50.0f);
//            //Rotate around external orbit point
//           // matrix.postRotate(rotation2, otherX, otherY);
//            //Translate to the draw location
//            matrix.postTranslate(200.0f, 220.0f);
//
//            canvas.drawBitmap(mImage, matrix, mPaint);
        }
    }



    }
