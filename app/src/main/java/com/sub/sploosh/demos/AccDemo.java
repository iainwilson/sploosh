package com.sub.sploosh.demos;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.sub.sploosh.R;

/**
 * Created by Liam Mullen on 24/02/2016.
 */
public class AccDemo extends Activity implements SensorEventListener {

    SensorManager sm;
    Sensor accelerometer;
    TextView acceleration;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acc_lay);
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        acceleration = (TextView)findViewById(R.id.acceleration);

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
    acceleration.setText("X: "+event.values[0]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
