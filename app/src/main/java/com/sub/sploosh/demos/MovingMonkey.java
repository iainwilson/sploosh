package com.sub.sploosh.demos;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.app.Activity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.support.v4.app.NavUtils;
import android.widget.TextView;

import com.sub.sploosh.R;

/**
 * Created by Fiona and Liam on 01/12/2015.
 */
public class MovingMonkey extends Activity {

    ImageView imageView;
    TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moving_monkey);
        imageView = (ImageView)findViewById(R.id.touch_input_imageview);
        textView = (TextView)findViewById(R.id.touch_input_event_toucharea);



        textView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                int eid = event.getAction();
                switch (eid) {
                    case MotionEvent.ACTION_MOVE:

                        RelativeLayout.LayoutParams mParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                        int x = (int) event.getRawX();
                    //    int y = (int) event.getRawY();
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int width = size.x;
                      //  int height = size.y;
                      //  mParams.leftMargin = x-50;
                        mParams.topMargin = 150;
                        if (x <width-imageView.getWidth())
                        {
                            mParams.leftMargin = x;
                        }

                        imageView.setLayoutParams(mParams);


                        break;

                    default:
                        break;
                }
                return true;
            }
        });
    }




}







