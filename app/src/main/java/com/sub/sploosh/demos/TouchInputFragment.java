package com.sub.sploosh.demos;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sub.sploosh.R;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Liam Mullen
 */


/**
 * A placeholder fragment containing a simple view.
 */
public class TouchInputFragment extends Fragment {

    /**
     * Media player that will be used to play the music clip
     */
    private MediaPlayer mMediaPlayer;

    /**
     * Boolean flag used to indicate when playback can commence
     */
    private boolean mMediaAvailable = false;

	/*
	 * (non-Javadoc)
	 *
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */

    /*
      * (non-Javadoc)
      * @see android.app.Fragment#onCreateView(android.view.LayoutInflater,
      * android.view.ViewGroup, android.os.Bundle)
      */

    AssetManager assetManager;
    View view;
    ImageView imageView;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the view
        view = inflater.inflate(R.layout.touch_input_fragment,
                container, false);

        // Get the asset manager for the current activity and load in a
        // text and bitmap asset

        assetManager = getActivity().getAssets();

        Bitmap bitmap = loadBitmap(assetManager, "imgLiam/monkey.png");
        Bitmap bitmap2 = loadBitmap(assetManager, "imgLiam/monkeyglass.png");

        // Get the 'touch' area and add a touch listener to it
        TextView touchArea = (TextView) view
                .findViewById(R.id.touch_input_event_toucharea);
        touchArea.setOnTouchListener(new SingleTouchListener());

        imageView = (ImageView) view
                .findViewById(R.id.touch_input_imageview);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }

        // Direct volume change requests to the audio manager
        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // Create a new media player and try to load/prep the music clip
        mMediaPlayer = new MediaPlayer();
        try {
            // Create a suitable file descriptor to the music cli
            AssetFileDescriptor musicDescriptor = assetManager
                    .openFd("music/sandstorm.mp3");

            // Get the media player ready to play the music clip
            mMediaPlayer.setDataSource(musicDescriptor.getFileDescriptor(),
                    musicDescriptor.getStartOffset(),
                    musicDescriptor.getLength());
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepare();

            // Indicate that the media is available to be played
            mMediaAvailable = true;
        } catch (IOException e) {
            // If we have any problems loading the music then indicate this
            TextView outputText = (TextView) view
                    .findViewById(R.id.touch_input_event_textview);
            outputText.setText("ERROR: Problem playing music. "
                    + e.getMessage());
        }

        return view;
    }

    // ////////////////////////////////////////////////////////////////////////
    // Single Touch Event Handler
    // ////////////////////////////////////////////////////////////////////////

    /**
     * Listener class that can support a single touch event (not that useful but
     * it acts as a basis on which a multi-touch listener can be constructed).
     */
    @SuppressLint("ClickableViewAccessibility")
    private class SingleTouchListener implements OnTouchListener {

        // ////////////////////////////////////////////////////////////////////
        // Touch Events
        // ////////////////////////////////////////////////////////////////////

        /**
         * Remember the last motion event that occurred (used to determine if a
         * move event has concluded and to limit the number of events that are
         * output).
         */
        private int mLastMotionEvent = MotionEvent.ACTION_CANCEL;

        /*
         * (non-Javadoc)
         *
         * @see android.view.View.OnTouchListener#onTouch(android.view.View,
         * android.view.MotionEvent)
         */
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            // Extract the single-touch event type and location
            int eventType = event.getActionMasked();
            float x = event.getX();
            float y = event.getY();

            // Display the touch position
            displayTouchPosition(x, y);

            // If we have a change of touch event (down to move, move to up,
            // etc.) then display relevant information
            if (mLastMotionEvent != eventType) {

                switch (eventType) {
                    case MotionEvent.ACTION_DOWN:
                        displayEvent("Down", x, y);
                        mMediaPlayer.start();
                        Bitmap bitmap2 = loadBitmap(assetManager, "imgLiam/monkeyglass.png");


                        imageView = (ImageView) view
                                .findViewById(R.id.touch_input_imageview);
                        if (bitmap2 != null) {
                            imageView.setImageBitmap(bitmap2);
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        displayEvent("Move Start", x, y);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mLastMotionEvent == MotionEvent.ACTION_MOVE) {
                            displayEvent("Move End", x, y);
                            mMediaPlayer.pause();
                            Bitmap bitmap = loadBitmap(assetManager, "imgLiam/monkey.png");
                            imageView = (ImageView) view
                                    .findViewById(R.id.touch_input_imageview);
                            if (bitmap != null) {
                                imageView.setImageBitmap(bitmap);
                            }
                        }
                        displayEvent("Up", x, y);
                        mMediaPlayer.pause();
                        break;
                }
            }

            // Remember the last event type that occurred
            mLastMotionEvent = eventType;

            return true;
        }

        // ////////////////////////////////////////////////////////////////////
        // Touch Info Display
        // ////////////////////////////////////////////////////////////////////

        /**
         * String builder to help put together the output strings
         */
        private StringBuilder mEventDetails = new StringBuilder();

        /**
         * Display the x and y touch location
         *
         * @param x
         *            Location of the touch event on the x axis
         * @param y
         *            Location of the touch event on the y axis
         */
        private void displayTouchPosition(float x, float y) {
            TextView xLoc = (TextView) getView().findViewById(
                    R.id.touch_input_X_loc);
            xLoc.setText(String.format("%.1f", x));
            TextView yLoc = (TextView) getView().findViewById(
                    R.id.touch_input_Y_loc);
            yLoc.setText(String.format("%.1f", y));
        }

        /**
         * Display details of the event that has occurred
         *
         * @param event
         *            Event descriptor
         * @param x
         *            Location of event along x-axis
         * @param y
         *            Location of event along y-axis
         */
        private void displayEvent(String event, float x, float y) {

            mEventDetails.append(String.format("%-15s", event));
            mEventDetails.append(" [");
            mEventDetails.append(String.format("%.1f", x));
            mEventDetails.append(",");
            mEventDetails.append(String.format("%.1f", y));
            mEventDetails.append("]\n");

            TextView eventTextView = (TextView) getView().findViewById(
                    R.id.touch_input_event_textview);
            eventTextView.setText(mEventDetails.toString());
        }
    }

    private Bitmap loadBitmap(AssetManager assetManager, String asset) {

        Bitmap bitmap = null;
        InputStream inputStream = null;

        try {
            // Try to open the bitmap
            inputStream = assetManager.open(asset);

            // Setup what load preferences we might have (this could be an
            // argument)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            // Load the bitmap
            bitmap = BitmapFactory.decodeStream(inputStream, null, options);

        } catch (IOException e) {
            Log.e("gp9",
                    "Error loading bitmap: " + e.getMessage());
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) { /* Let's just return with a null */ }
        }

        return bitmap;
    }
}
