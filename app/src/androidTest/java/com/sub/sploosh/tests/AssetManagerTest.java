package com.sub.sploosh.tests;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.sub.sploosh.engine.AssetManager;
import com.sub.sploosh.engine.FileIO;

import org.junit.Test;

/**
 * Created by Jason on 15/03/16.
 */

@SmallTest
public class AssetManagerTest extends InstrumentationTestCase {

    private AssetManager assetManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        Context context = getInstrumentation().getTargetContext();
        assetManager = new AssetManager(new FileIO(context));
    }

    @Test
    public void loadAndAddBitmapSuccess()
    {
        assertTrue(assetManager.loadAndAddBitmap("bird", "redBird.png"));
    }

    @Test
    public void loadAndAddMusicSuccess()
    {
        assertTrue(assetManager.loadAndAddBitmap("Spyro", "audio/Spyro.mp3"));
    }

    @Test
    public void loadAndAddSoundSuccess()
    {
        assertTrue(assetManager.loadAndAddBitmap("sploosh", "audio/sploosh.mp3"));
    }

    @Test
    public void testLoadAndAddAndGetBitmapSuccess()
    {
        assertNotNull(assetManager.loadAndAddAndGetBitmap("bird", "redBird.png"));
    }

    @Test
    public void testLoadAndAddAndGetMusicSuccess()
    {
        assertNotNull(assetManager.loadAndAddAndGetMusic("Spyro", "audio/Spyro.mp3"));
    }

    @Test
    public void testLoadAndAddAndGetSoundSuccess()
    {
        assertNotNull(assetManager.loadAndAddAndGetSound("sploosh", "audio/sploosh.mp3"));
    }

}
